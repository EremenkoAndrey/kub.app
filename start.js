require('./src/server/tools/babel-preprocessing');
require('./src/server/aliases');
const debug = require('debug')('KUB:server');
const { server, port, host } = require('./src/server/');

server.listen(port, host);

server.on('error', (error) => {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? `Pipe ${port}`
        : `Pipe ${port}`;

    // handle specific listen errors with friendly messages
    switch (error.code) {
    case 'EACCES':
        console.error(`${bind} requires elevated privileges`);
        process.exit(1);
        break;
    case 'EADDRINUSE':
        console.error(`${bind} is already in use`);
        process.exit(1);
        break;
    default:
        throw error;
    }
});

server.on('listening', () => {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? `pipe ${addr}`
        : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
    console.log(addr);
    console.log(`Server allow on http://${host}:${addr.port}, env ${process.env.NODE_ENV}`);
});
