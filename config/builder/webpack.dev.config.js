module.exports = {
    output: {
        publicPath: '/'
    },
    watch: true,
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './public'
    }
};
