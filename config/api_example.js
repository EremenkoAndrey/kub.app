const api = {
    // Домен API
    host: 'test.kub.media',

    // Протокол API
    protocol: 'https',

    // Использовать абсолютные пути для fetch-запросов с клиента
    useAbsPathsForClientRequests: false,

    widget: '/fapi/v1/document/widget/',

    // Листинги
    listing: '/fapi/v1/document/listing/',

    // Документ
    document: '/fapi/v1/document/view/',

    // Поиск
    search: '/fapi/v1/document/search/'
};

export default api;
