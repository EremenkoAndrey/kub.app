const app = {
    // Название сайта
    siteName: 'KYБ',

    // Домен сайта
    domain: 'test.kub.media',

    // Хост приложения
    host: '127.0.0.1',
    // Порт приложения
    port: '8080',

    // Протокол сайта
    protocol: 'https',

    vendors: {
        hypercomments: {
            id: 104767
        }
    }
};

export default app;
