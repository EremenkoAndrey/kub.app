import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import { FETCH_LISTING } from 'actions/listings';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies, import/first */


class Listing extends React.Component {
    constructor(props) {
        super(props);
        const { excludeDocs, excludeWidgets, storeWidgets } = props;
        this.debt = 0;
        this.loadingDocs = this.debt;

        this.state = {
            hasNextPage: props.listing ? !!props.listing.nextPage : null,
            // другие статусы: 'loading', 'error' - ошибка, 'done' -  элементов больше нет
            status: '',
            exclude: Listing.getExcludeList(excludeDocs, excludeWidgets, storeWidgets),
            page: 1
        };
    }

    componentDidMount() {
        if (this.debt && this.state.hasNextPage !== false) {
            this.loadMore(this.debt);
        }
    }

    componentDidUpdate() {
        if (this.debt && this.state.hasNextPage !== false) {
            this.loadMore(this.debt);
        } else if (this.debt === 0 || this.state.hasNextPage === false) {
            this.clearLoading();
        }
    }

    componentWillUnmount() {
        this.clearLoading();
    }

    static getDerivedStateFromProps(nextProps) {
        let status = '';
        const {
            listing, error, excludeDocs, excludeWidgets, storeWidgets
        } = nextProps;
        const hasNextPage = (listing && listing.nextPage) ? !!listing.nextPage : null;

        if (listing && !listing.nextPage) {
            status = 'done';
        }
        if (error) {
            status = 'error';
        }

        return {
            hasNextPage,
            status,
            exclude: Listing.getExcludeList(excludeDocs, excludeWidgets, storeWidgets)
        };
    }

    // Возвращает сортированный список ID элементов, которые необходимо исключить
    static getExcludeList(docs, widgetsNames, storeWidgets) {
        let ids = [];

        if (widgetsNames.length && storeWidgets) {
            const reducedElements = widgetsNames.reduce((acc, widgetName) => {
                if (storeWidgets[widgetName] && storeWidgets[widgetName].docs) {
                    return acc.concat(storeWidgets[widgetName].docs);
                }
                return acc;
            }, []);
            ids = ids.concat(reducedElements);
        }

        if (docs.length) {
            ids = ids.concat(docs);
        }

        return Array.from(new Set(ids)).sort();
    }

    getMore = () => {
        this.setState({
            page: this.state.page += 1
        });
    };

    addLoading(debt) {
        this.loadingDocs = debt;
        this._timer = setTimeout(() => {
            if (this.loadingDocs === debt) {
                this.setState({
                    status: 'loading'
                });
            }
        }, 200);
    }

    clearLoading() {
        this.loadingDocs = this.debt;
        clearTimeout(this._timer);
    }

    // Принимает как аргумент текущее количество недостающих элментеов
    // это количество затем используется для определения того, что загрузка
    // новых элементов состоялась и статус 'loading' можно снимать
    loadMore(debt) {
        if (this.loadingDocs === debt) {
            return;
        }
        this.addLoading(debt);
        const { name, quantity } = this.props;
        this.props.loadMore(name, quantity);
    }

    selectItems() {
        const { quantity, listing } = this.props;
        const { page } = this.state;
        const shouldBeSize = quantity * page;

        const filteredElements = listing ?
            listing.docs.filter(id => this.state.exclude.indexOf(id) === -1) :
            [];

        if (filteredElements.length <= shouldBeSize) {
            this.debt = shouldBeSize - filteredElements.length;
            return filteredElements;
        }
        this.debt = 0;
        return filteredElements.slice(0, shouldBeSize);
    }

    render() {
        const child = React.Children.only(this.props.children);
        return React.cloneElement(child, {
            docs: this.selectItems(),
            status: this.state.status,
            getMore: this.getMore
        });
    }
}

Listing.propTypes = {
    name: PropTypes.string.isRequired,
    children: PropTypes.element.isRequired,
    listing: PropTypes.shape({
        nextPage: PropTypes.string,
        docs: PropTypes.arrayOf(PropTypes.string)
    }),
    loadMore: PropTypes.func,
    quantity: PropTypes.number,
    excludeDocs: PropTypes.arrayOf(PropTypes.string),
    excludeWidgets: PropTypes.arrayOf(PropTypes.string),
    // eslint-disable-next-line
    storeWidgets: PropTypes.object
};

Listing.defaultProps = {
    loadMore: null,
    listing: null,
    quantity: 12,
    excludeWidgets: [],
    excludeDocs: [],
    storeWidgets: null
};

const mapStateToProps = (state, ownProps) => (
    {
        listing: state.listings[ownProps.name],
        storeWidgets: state.widgets,
        name: ownProps.name
    }
);


const mapDispatchToProps = dispatch => ({
    loadMore: (listingId, quantity) => {
        dispatch(FETCH_LISTING(listingId, quantity));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Listing);
