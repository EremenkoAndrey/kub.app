import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'modules/Config';
import RedirectPage from 'nodes/RedirectPage/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */


const RedirectTo = ({ url }) => {
    // Этот код исполняется только в браузере
    const parsedUrl = new URL(url, window.location);
    const appHost = Config.get('app.domain');
    if (parsedUrl.host === appHost) {
        return <Redirect to={parsedUrl.pathname} />;
    }
    window.location = url;
    return <RedirectPage url={url} />;
};

RedirectTo.propTypes = {
    url: PropTypes.string.isRequired
};

export default RedirectTo;
