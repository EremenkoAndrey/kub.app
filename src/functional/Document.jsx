import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Typings from 'utils/Typings';
import RedirectTo from 'functional/RedirectTo';
import ErrorPage from 'nodes/ErrorPage/';
import Loader from 'components/Loader/';
import { FETCH_DOCUMENT } from 'actions/docs';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */


const Document = ({
    doc, view: Component, fetchDocument, match
}) => {
    if (!doc) {
        const _urlParams = match.url.split('/').filter(param => !!param);
        fetchDocument(_urlParams);
        return <Loader />;
    } else if (doc.type === 'Redirect') {
        return <RedirectTo url={doc.to} />;
    } else if (doc.redirect) {
        return <RedirectTo url={doc.redirect.to} />;
    } else if (doc.type === 'Error') {
        return <ErrorPage />;
    }
    return <Component doc={doc} />;
};

Document.propTypes = {
    view: PropTypes.func.isRequired,
    fetchDocument: PropTypes.func.isRequired,
    match: PropTypes.shape({
        url: PropTypes.string
    }).isRequired,
    doc: PropTypes.oneOfType([
        Typings.newsDocument(),
        Typings.tagDocument(),
        Typings.redirectDocument(),
        Typings.errorDocument()
    ])
};

Document.defaultProps = {
    doc: null
};

const mapStateToProps = (state, ownProps) => ({
    doc: state.docs[ownProps.id]
});

const mapDispatchToProps = dispatch => ({
    fetchDocument: (id) => {
        dispatch(FETCH_DOCUMENT(id));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Document);
