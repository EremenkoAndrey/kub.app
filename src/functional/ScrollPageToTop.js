import React from 'react';
import PropTypes from 'prop-types';

export default class ScrollPageToTop extends React.Component {
    static scrollToTop() {
        if (typeof window !== 'undefined') {
            window.scroll(0, 0);
        }
    }
    componentDidMount() {
        ScrollPageToTop.scrollToTop();
    }
    componentDidUpdate(prevProps) {
        if (!this.props.id) {
            return;
        }
        if (this.props.id !== prevProps.id) {
            ScrollPageToTop.scrollToTop();
        }
    }

    render() {
        return null;
    }
}

ScrollPageToTop.propTypes = {
    id: PropTypes.string
};

ScrollPageToTop.defaultProps = {
    id: ''
};
