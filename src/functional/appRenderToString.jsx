import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import thunk from 'redux-thunk';
import { applyMiddleware } from 'redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import RootRoutes from 'routes/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import configureStore from './../store';


export default function appRenderToString(url, initialState = {}, context = {}) {
    const store = configureStore(initialState, applyMiddleware(thunk));
    return renderToString((
        <Provider store={store}>
            <StaticRouter location={url} context={context}>
                <RootRoutes />
            </StaticRouter>
        </Provider>
    ));
}
