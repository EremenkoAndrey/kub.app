import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import zfill from 'utils/zfill';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

// Принимает количество милисекунд, возвращает форматированную строку
const TimeFormatted = ({ monthDictionary, ms }) => {
    const fullDate = new Date(parseInt(ms, 10));
    // Для корректной проверки на NaN
    if (Number.isNaN(Date.parse(fullDate))) {
        return '';
    }
    const day = fullDate.getDate();
    const month = fullDate.getMonth();
    const year = fullDate.getFullYear();
    const hours = zfill(fullDate.getHours(), 2);
    const minutes = zfill(fullDate.getMinutes(), 2);

    return `${day} ${monthDictionary[month]} ${year !== TimeFormatted.currentYear ? `${year} г.` : ','} ${hours}:${minutes}`;
};

TimeFormatted.currentYear = new Date().getFullYear();

TimeFormatted.propTypes = {
    ms: PropTypes.string,
    monthDictionary: PropTypes.arrayOf(PropTypes.string)
};

TimeFormatted.defaultProps = {
    ms: '',
    monthDictionary: [
        'января',
        'февраля',
        'марта',
        'апреля',
        'мая',
        'июня',
        'июля',
        'августа',
        'сентября',
        'октября',
        'ноября',
        'декабря'
    ]
};

export default TimeFormatted;
