import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import { FETCH_WIDGET } from 'actions/widgets';
import Typings from 'utils/Typings';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */


class Widget extends React.Component {
    componentDidMount() {
        // Сработает только на клиенте
        if (!this.props.widget) {
            this.props.getWidget(this.props.id);
        }
    }

    render() {
        const child = React.Children.only(this.props.children);

        return this.props.widget ?
            React.cloneElement(child, {
                docs: this.props.widget.docs || []
            }) :
            null;
    }
}

Widget.propTypes = {
    getWidget: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
    children: PropTypes.element,
    widget: Typings.widget()
};

Widget.defaultProps = {
    widget: null,
    children: null
};

const mapDispatchToProps = dispatch => ({
    getWidget: (id) => {
        dispatch(FETCH_WIDGET(id));
    }
});

const mapStateToProps = (state, ownProps) => ({
    widget: state.widgets[ownProps.name],
    id: ownProps.name
});

export default connect(mapStateToProps, mapDispatchToProps)(Widget);
