/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Middleware from 'modules/Middleware';
import widgetResponse from 'middleware/widgetResponse';
import listingResponse from 'middleware/listingResponse';
import documentResponse from 'middleware/documentResponse';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const middleware = new Middleware();

// Общие обработчики для определенных endpoints
middleware.get('widget', null, widgetResponse);
middleware.get('listing', null, listingResponse);
middleware.get('document', null, documentResponse);
middleware.get('preview', null, documentResponse);

export default middleware;
