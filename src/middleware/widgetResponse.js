/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Adapter from 'modules/Adapter';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

export default function widgetResponse(err, res) {
    if (err) {
        return err;
    }
    if (!res.data) {
        /* eslint-disable-next-line */
        console.error('Incorrect widget response');
        res.data = {
            docs: []
        };
    }
    if (res.data.error_message) {
        /* eslint-disable-next-line */
        console.error(`Widget response error: ${res.data.error_message}`);
    }

    const docs = Array.isArray(res.data.docs) ?
        res.data.docs.reduce((acc, doc) => {
            if (doc.type === 'News') {
                const news = Adapter.toNewsDocument(doc);
                acc[news.id] = news;
            } else if (doc.type === 'Tag') {
                const tag = Adapter.toTagDocument(doc);
                acc[tag.id] = tag;
            } else if (doc.type === 'Banner') {
                const banner = Adapter.toBannerDocument(doc);
                acc[banner.id] = banner;
            }
            return acc;
        }, {}) :
        {};

    return {
        widget: {
            title: res.data.title || '',
            titleLink: res.data.titleLink || '',
            docs: Object.keys(docs)
        },
        docs
    };
}
