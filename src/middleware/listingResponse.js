/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Adapter from 'modules/Adapter';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

export default function listingResponse(err, res) {
    if (err) {
        return err;
    }
    if (!res.data) {
        /* eslint-disable-next-line */
        console.error('Incorrect listing response');
        res.data = {
            docs: []
        };
    }
    if (res.data.error_message) {
        /* eslint-disable-next-line */
        console.error(`Listing response error: ${res.data.error_message}`);
    }
    const docs = Array.isArray(res.data.docs) ?
        res.data.docs.reduce((acc, doc) => {
            if (doc.type === 'News') {
                const news = Adapter.toNewsDocument(doc);
                acc[news.id] = news;
            } else if (doc.type === 'Tag') {
                const news = Adapter.toTagDocument(doc);
                acc[news.id] = news;
            }
            return acc;
        }, {}) :
        {};

    return {
        listing: {
            total: res.data.total || null,
            nextPage: res.data.nextPage || '',
            docs: Object.keys(docs)
        },
        docs
    };
}
