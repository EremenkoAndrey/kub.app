/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Adapter from 'modules/Adapter';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

export default function documentResponse(err, res) {
    if (err) {
        return Adapter.toErrorDocument(err);
    }
    /* Эта ошибка может возникнуть только при отладке, если неправильно обратиться к API */
    if (res.data.error_message) {
        /* eslint-disable-next-line */
        console.error(`Document response error: ${res.data.error_message}`);
    }
    if (!res.data || !(res.data.document || res.data.redirectTo)) {
        /* eslint-disable-next-line */
        console.error('Incorrect document response');
        return null;
    }

    if (res.data.redirectTo) {
        return Adapter.toRedirect(res.data);
    }
    switch (res.data.document.type) {
    case 'News':
        return Adapter.toNewsDocument(res.data.document);
    case 'Tag':
        return Adapter.toTagDocument(res.data.document);
    default:
        /* eslint-disable-next-line */
        console.error('Incorrect document type');
        return null;
    }
}
