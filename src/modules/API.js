/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import ServerConnect from 'modules/ServerConnect';
import middleware from 'middleware/';
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

/**
 * Отправка:
 *
 * API.post('auth')
 *
 */

export default class API {
    static get hostUrl() {
        if (!this._hostUrl) {
            this._hostUrl = `${Config.get('api.protocol')}://${Config.get('api.host')}`;
        }
        return this._hostUrl;
    }
    static get serverConnect() {
        if (!this._serverConnect) {
            this._serverConnect = ServerConnect;
        }
        return this._serverConnect;
    }
    static get middleware() {
        if (!this._middleware) {
            this._middleware = middleware;
        }
        return this._middleware;
    }

    /**
     * Проксирование GET-запроса на сервер
     *
     * @param entryName - (string) required, имя точки входа
     * (под ним зарегистрирован URI в конфиг-файле)
     * @param params - (string|object|array) парметры,
     * которые будут переданы на сервер, массив преобразуется
     * в строку из его значений, разделенных символом /,
     * объект преобразуется в get-параметры, передаваемые
     * после символа ? и разделенные символом &
     * @returns Promise
     */
    static get(entryName, params, absPath = true) {
        const fullPath = this._getFullPath(entryName, absPath);
        const { requestMiddleware, responseMiddleware } = this._getMiddleware('get', entryName);
        return this.serverConnect.get(fullPath, params, requestMiddleware, responseMiddleware);
    }
    static post(entryName, params, absPath = true) {
        const fullPath = this._getFullPath(entryName, absPath);
        const { requestMiddleware, responseMiddleware } = this._getMiddleware('post', entryName);
        return this.serverConnect.post(fullPath, params, requestMiddleware, responseMiddleware);
    }
    static put() {

    }
    static del() {

    }
    static register() {
        return this.middleware;
    }
    static _getFullPath(entryName, absPath) {
        const endPoint = Config.get(`api.${entryName}`, null);
        if (endPoint === null && process.env.NODE_ENV !== 'production') {
            throw new Error(`API entry point ${entryName} does not exist in the config file`);
        }
        return `${absPath ? this.hostUrl : ''}${endPoint}`;
    }
    static _getMiddleware(methodName, entryName) {
        const requestMiddleware = this.middleware.findRequest(methodName, entryName);
        const responseMiddleware = this.middleware.findResponse(methodName, entryName);
        return { requestMiddleware, responseMiddleware };
    }
}
