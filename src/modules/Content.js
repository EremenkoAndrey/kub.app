import cheerio from 'cheerio';

export default class Content {
    constructor(html, tmpl = { code: () => {}, image: () => {} }) {
        this.$ = cheerio.load(html, { decodeEntities: false });
        this.template = tmpl;
    }

    insertMedia(media) {
        const mediaElements = this.$('rtmedia');

        mediaElements.each((index, element) => {
            const mediaId = this.$(element).attr('data-id');
            const mediaObject = media.find(mediaData => mediaData.id === mediaId);
            if (!mediaObject) return;

            switch (mediaObject.type.toLowerCase()) {
            case 'code':
                this._replaceTag(element, this.template.code(mediaObject));
                break;
            case 'image':
                this._replaceTag(element, this.template.image(mediaObject));
                break;
            default:
                break;
            }
        });
        return this.$('body').html();
    }

    _replaceTag(mediaTag, template) {
        this.$(template).insertBefore(mediaTag);
        this.$(mediaTag).remove();
    }
}
