/*
Метода класса получают в аргументе данные, пришедшие с сервера
и преобразуют в объект, формата, который ожидает приложение.
Этот формат описывается в файле utils/Typings.js. Соответственно,
любые изменения в этом файле, должны отражаться в файле Typings.js
 */

export default class Adapter {
    static toRedirect(resData) {
        return {
            type: 'Redirect',
            to: resData.redirectTo || '/',
            code: resData.redirectCode || 301
        };
    }

    static toErrorDocument(err) {
        return {
            type: 'Error',
            message: err.message || 'Unknown error',
            status: err.status || null
        };
    }

    static toNewsDocument(doc) {
        this.checkDocType(doc, 'News');
        return {
            _id: doc.id,
            id: doc.href || `/preview/${doc.id}/`,
            type: 'News',
            title: doc.title || '',
            summary: doc.summary || '',
            text: doc.text || '',
            href: doc.href || '',
            slug: doc.slug || '',
            updatedAt: doc.updatedAt || '',
            publishedAt: doc.publishedAt || '',
            metaTitle: doc.metaTitle || '',
            metaDescription: doc.metaDescription || '',
            redirect: doc.redirectTo ? this.toRedirect(doc) : null,
            tags: doc.tags ? doc.tags.map(tag => ({
                id: tag.id || 'no id',
                title: tag.title || '',
                slug: tag.slug || '',
                href: tag.href || ''
            })) : [],
            media: doc.media ? doc.media.map(mediaElement => (
                {
                    id: mediaElement.id || 'no id',
                    type: mediaElement.type || 'no type',
                    body: {
                        raw: mediaElement.paths.raw || null,
                        article: mediaElement.paths.article || '',
                        thumbnail: mediaElement.paths.thumbnail || '',
                        crop: mediaElement.paths.crop || '',
                        original: mediaElement.paths.original || ''
                    },
                    provider: mediaElement.provider || '',
                    source: mediaElement.source || '',
                    copyright: mediaElement.copyright || '',
                    title: mediaElement.title || '',
                    main: mediaElement.main || false
                }
            )) : []
        };
    }

    static toTagDocument(doc) {
        this.checkDocType(doc, 'Tag');
        return {
            _id: doc.id,
            id: doc.href || `/tags/${doc.id}/`,
            type: 'Tag',
            title: doc.title || '',
            summary: doc.summary || '',
            href: doc.href || '',
            slug: doc.slug || '',
            updatedAt: doc.updatedAt || '',
            publishedAt: doc.publishedAt || '',
            metaTitle: doc.metaTitle || '',
            metaDescription: doc.metaDescription || '',
            redirect: doc.redirectTo ? this.toRedirect(doc) : null
        };
    }

    static toBannerDocument(doc) {
        this.checkDocType(doc, 'Banner');
        return {
            _id: doc.id,
            id: `/banner/${doc.id}/`,
            type: 'Banner',
            title: doc.title || '',
            summary: doc.summary || '',
            href: doc.redirectTo || '',
            slug: doc.slug || '',
            updatedAt: doc.updatedAt || '',
            publishedAt: doc.publishedAt || '',
            openLinkInNewWindow: !!doc.openLinkInNewWindow,
            media: doc.media ? doc.media.map(mediaElement => (
                {
                    id: mediaElement.id,
                    type: mediaElement.type || 'no type',
                    body: {
                        raw: mediaElement.paths.raw || null,
                        original: mediaElement.paths.original || ''
                    },
                    source: mediaElement.source || '',
                    copyright: mediaElement.copyright || '',
                    title: mediaElement.title || '',
                    main: mediaElement.main || false
                }
            )) : []
        };
    }
    static checkDocType(doc, type) {
        if (process.env.NODE_ENV !== 'production') {
            if (doc.type !== type) {
                /* eslint-disable-next-line */
                console.warn(`Ожидался документ типа ${type}, но пришел документ другого типа (${doc.type}).
                Это может привести к ошибке`);
            }
        }
    }
}
