/**
 * Добавление middleware:
 *
 * const register = new Middleware();
 * register.get('auth', req => req, res => res)
 *
 */

export default class Middleware {
    constructor() {
        this._middleware = {};
    }

    _register({
        method, entryName, req, res
    }) {
        const _method = method.toLowerCase();
        if (!this._middleware[entryName]) {
            this._middleware[entryName] = {};
        }
        this._middleware[entryName][_method] = {
            req: req ? (request) => {
                try {
                    return req(request);
                } catch (err) {
                    return new Error('Ошибка при вызове request middleware функции');
                }
            } : null,
            res: res ? (error, response, request) => {
                try {
                    return res(error, response, request);
                } catch (err) {
                    return new Error('Ошибка при вызове response middleware функции');
                }
            } : null
        };
    }

    /**
     * Найти middleware запроса
     *
     * @param method - (string) Метод (GET, POST...)
     * (под ним зарегистрирован URI в конфиг-файле)
     * @param entryName - (string) required, имя точки входа
     * (под ним зарегистрирован URI в конфиг-файле)
     * @return function|null
     */
    findRequest(method, entryName) {
        const point = this._middleware[entryName];
        const _method = method.toLowerCase();
        if (point && point[_method] && point[_method].req) {
            return point[_method].req;
        }
        return null;
    }

    /**
     * Найти middleware ответа сервера
     *
     * @param method - (string) Метод (GET, POST...)
     * (под ним зарегистрирован URI в конфиг-файле)
     * @param entryName - (string) required, имя точки входа
     * (под ним зарегистрирован URI в конфиг-файле)
     * @return function|null
     */
    findResponse(method, entryName) {
        const point = this._middleware[entryName];
        const _method = method.toLowerCase();
        if (point && point[_method] && point[_method].res) {
            return point[_method].res;
        }
        return null;
    }

    /**
     * Зарегистрировать middleware для GET-запроса
     *
     * @param entryName - (string) required, имя точки входа
     * (под ним зарегистрирован URI в конфиг-файле)
     * @param req - function
     * @param res - function
     * @returns Promise
     */
    get(entryName, req, res) {
        this._register({
            method: 'get',
            entryName,
            req,
            res
        });
    }
    post(entryName, req, res) {
        this._register({
            method: 'post',
            entryName,
            req,
            res
        });
    }
    put(entryName, req, res) {
        this._register({
            method: 'put',
            entryName,
            req,
            res
        });
    }
    del(entryName, req, res) {
        this._register({
            method: 'del',
            entryName,
            req,
            res
        });
    }
}
