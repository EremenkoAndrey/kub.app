export default class ServerConnect {
    /**
     * GET-запрос на сервер
     *
     * @param endPoint - (string) required
     * @param params - (string|object|array)
     * @param requestMiddleware - function
     * @param responseMiddleware - function
     * @returns Promise
     */
    static get(endPoint, params = '', requestMiddleware = null, responseMiddleware = null) {
        // Если нужна предобработка запроса функцией requestMiddleware
        const _params = (requestMiddleware) ? requestMiddleware(params) : params;
        let _url;
        // Формируем GET строку из параметров
        if (typeof _params === 'string') {
            _url = _params.length ? `${endPoint}/${_params}` : endPoint;
        } else if (Array.isArray(_params)) {
            _url = _params.length ? _params.reduce((acc, param) => `${acc}${param}/`, endPoint) : endPoint;
        } else {
            _url = Object.keys(_params).reduce((acc, key, index) => {
                const separator = (index > 0) ? '&' : '';
                return `${acc}${separator}${key}=${_params[key]}`;
            }, `${endPoint}?`);
        }

        return new Promise((resolve, reject) => {
            fetch(_url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then((res) => {
                    if (res.ok) {
                        return res.json(res);
                    }
                    const error = {
                        status: res.status,
                        message: res.statusText
                    };
                    throw new Error(JSON.stringify(error));
                })
                .then(json => resolve(responseMiddleware ?
                    responseMiddleware(null, json, params) :
                    json))
                .catch(err => reject(responseMiddleware ?
                    responseMiddleware(JSON.parse(err.message)) :
                    JSON.parse(err.message)));
        });
    }

    /**
     * POST-запрос на сервер
     *
     * @param entryPoint - (string) required
     * @param params - (string|object)
     * @param requestMiddleware - function
     * @param responseMiddleware - function,
     * @param method - (string)
     * @returns Promise
     */
    static post(entryPoint, params = '', requestMiddleware = null, responseMiddleware = null, method = 'POST') {
        // Если нужна предобработка запроса функцией requestMiddleware
        const _params = (requestMiddleware) ? requestMiddleware(params) : params;
        const body = (typeof _params === 'string') ? _params : JSON.stringify(_params);

        return new Promise((resolve, reject) => {
            fetch(entryPoint, {
                method,
                headers: {
                    'Content-Type': 'application/json'
                },
                body
            })
                .then((res) => {
                    if (res.ok) {
                        return res.json(res);
                    }
                    const error = {
                        status: res.status,
                        message: res.statusText
                    };
                    throw new Error(JSON.stringify(error));
                })
                .then(json => resolve(responseMiddleware ?
                    responseMiddleware(null, json, params) :
                    json))
                .catch(err => reject(responseMiddleware ?
                    responseMiddleware(JSON.parse(err.message)) :
                    JSON.parse(err.message)));
        });
    }
    put(entryPoint, params = '', requestMiddleware = null, responseMiddleware = null) {
        return this.post(entryPoint, params, requestMiddleware, responseMiddleware, 'PUT');
    }
    del(entryPoint, params = '', requestMiddleware = null, responseMiddleware = null) {
        return this.post(entryPoint, params, requestMiddleware, responseMiddleware, 'DELETE');
    }
}
