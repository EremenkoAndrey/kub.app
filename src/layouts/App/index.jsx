import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import MainMenu from 'components/MainMenu/';
import Footer from 'components/Footer/';
import bemNaming from 'utils/bemNaming';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';


const App = ({
    children, copyright, bemNames, elem, mods
}) => (
    <div className={bemNames('app', elem, mods)}>
        <MainMenu elem={bemNames('', 'app__main-menu', mods)} />
        <div className={bemNames('', 'app__body', mods)}>
            <div className={bemNames('', 'app__content', mods)}>
                {children}
            </div>

            <Footer elem={bemNames('', 'app__footer', mods)} />

            <div className={bemNames('', 'app__copyright', mods)}>
                {copyright}
            </div>
        </div>
    </div>
);

App.propTypes = {
    children: PropTypes.element,
    copyright: PropTypes.string,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    bemNames: PropTypes.func
};

App.defaultProps = {
    children: null,
    copyright: 'Зарегистрировано Федеральной службой по надзору в сфере связи, информационных технологий и массовых коммуникаций - серия Эл № ФС77-72675 от 23 апреля 2018 г.',
    mods: [],
    elem: '',
    bemNames: bemNaming
};

export default App;

