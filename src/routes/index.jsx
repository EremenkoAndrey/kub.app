import React from 'react';
import { Route, Switch } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Document from 'functional/Document';
import Tags from 'nodes/Tags/';
import Index from 'nodes/Index/';
import NewsView from 'nodes/NewsView/';
import TagView from 'nodes/TagView/';
import SearchPage from 'nodes/SearchPage/';
import ErrorPage from 'nodes/ErrorPage/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const RootRoutes = () => (
    <Switch>
        <Route exact path="/" component={Index} />
        <Route exact path="/tags" component={Tags} />
        <Route
            exact
            path="/tags/:id"
            render={props => <Document view={TagView} id={props.match.url} {...props} />}
        />
        <Route
            exact
            path="/news/:id"
            render={props => <Document view={NewsView} id={props.match.url} {...props} />}
        />
        <Route
            exact
            path="/preview/:id/:key"
            render={props => <Document view={NewsView} id={props.match.url} {...props} />}
        />
        <Route exact path="/search" component={SearchPage} />
        <Route render={() => <ErrorPage status={404} />} />
    </Switch>
);

export default RootRoutes;
