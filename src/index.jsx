import React from 'react';
import { hydrate } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';
import './polyfills';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import RootRoutes from 'routes/';
import { initGTM, pushToGTM } from 'utils/googleTagManager';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import configureStore from './store';
import './index.less';

const history = createHistory();
const routersMiddleware = routerMiddleware(history);
const initialState = window.__APP_INITIAL_STATE__ || {};
delete window.__APP_INITIAL_STATE__;

const store = configureStore(initialState, composeWithDevTools(applyMiddleware(
    thunk,
    routersMiddleware
)));


hydrate(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <RootRoutes />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('kub')
);

// Google Tag Manager
initGTM();
history.listen(pushToGTM);
