import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import App from 'layouts/App/';
import ScrollPageToTop from 'functional/ScrollPageToTop';
import SearchResults from 'components/SearchResults/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

// Метаданные устанавливаютс в Search
const SearchPage = ({ location }) => {
    const query = location.search.match(/q=(.[^&]+)/);
    const queryString = Array.isArray(query) ? decodeURI(query[1]) : '';

    return (
        <App>
            <div className="search-page">
                <ScrollPageToTop />
                <div className="search-page__wrapper">
                    {/*<NavLink*/}
                        {/*to="/search/?q=obama"*/}
                    {/*>*/}
                        {/*Obama*/}
                    {/*</NavLink>*/}
                    <SearchResults queryString={queryString} />
                </div>
            </div>

        </App>
    );
};

SearchPage.propTypes = {};

SearchPage.defaultProps = {};

export default SearchPage;
