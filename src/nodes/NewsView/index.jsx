import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'modules/Config';
import Typings from 'utils/Typings';
import App from 'layouts/App/';
import Listing from 'functional/Listing';
import ScrollPageToTop from 'functional/ScrollPageToTop';
import Article from 'components/Article/';
import Card from 'components/Card/';
import Carousel from 'components/Carousel/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

// TODO Добавить обработчик ошибок
// TODO добавить компонент-обертку для предпросмотра
const mapStateToProps = (state, ownProps) => (
    {
        doc: state.docs[ownProps.id],
        mods: ['bg_gray', 'max_height']
    }
);

const NewsCard = connect(mapStateToProps)(Card);

const LastNews = ({ docs }) => (
    <React.Fragment>
        {docs.map(id => (
            <div
                key={id}
                className="news-view__listing-item"
            >
                <NewsCard id={id} />
            </div>
        ))}
    </React.Fragment>
);

LastNews.propTypes = {
    docs: PropTypes.arrayOf(PropTypes.string)
};

LastNews.defaultProps = {
    docs: []
};


const NewsView = ({ doc }) => {
    const baseHref = `${Config.get('app.protocol')}://${Config.get('app.domain')}`;
    const cover = doc.media ? doc.media.find(mediaElem => mediaElem.type.toLowerCase() === 'cover' && mediaElem.main) : undefined;
    const meta = {
        title: doc.title || doc.metaTitle,
        description: doc.metaDescription || doc.summary,
        image: cover ? cover.body.article || cover.body.original : `${baseHref}/cube_share.png`,
        url: `${baseHref}${doc.href}`,
        ampUrl: `${baseHref}/amp${doc.href}`
    };

    return (
        <App>
            <div className="news-view">
                <ScrollPageToTop id={doc.id} />
                <Helmet>
                    <title>{meta.title}</title>
                    <meta name="description" content={meta.description} />

                    <meta property="og:site_name" content={Config.get('app.siteName')} />
                    <meta property="og:locale" content="ru_RU" />

                    <meta property="og:type" content="article" />
                    <meta property="article:section" content="Новости" />

                    {doc.tags.map(tag => (<meta property="article:tag" key={tag.id} content={tag.title} />))}

                    <meta property="og:title" content={meta.title} />
                    <meta property="og:url" content={meta.url} />
                    <meta property="og:description" content={meta.description} />
                    <meta property="og:image" content={meta.image} />

                    <meta property="og:image:type" content="image/jpeg" />
                    <meta property="og:image:width" content="840" />
                    <meta property="og:image:height" content="450" />

                    <meta name="twitter:site" content="@Kub" />
                    <meta name="twitter:card" content="summary_large_image" />

                    <meta name="twitter:title" content={meta.title} />
                    <meta name="twitter:image" content={meta.image} />

                    <link rel="amphtml" href={meta.ampUrl} />
                </Helmet>

                <div className="news-view__content">
                    <div className="news-view__wrapper">
                        <div className="news-view__columns">

                            <div className="news-view__article">
                                <Article doc={doc} />
                            </div>

                            <div className="news-view__aside">
                                <Listing
                                    name="type.Content"
                                    quantity={4}
                                    excludeDocs={[doc.id]}
                                >
                                    <LastNews />
                                </Listing>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="news-view__bottom">
                    <div className="news-view__wrapper">

                        <Listing name="type.News">
                            <Carousel
                                view={props => (<div className="news-view__carousel-item"><Card {...props} /></div>)}
                                mods={['max_height']}
                                settings={{
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    responsive: [
                                        {
                                            breakpoint: 1024,
                                            settings: {
                                                slidesToShow: 2,
                                                slidesToScroll: 2
                                            }
                                        },
                                        {
                                            breakpoint: 590,
                                            settings: {
                                                slidesToShow: 1,
                                                slidesToScroll: 1
                                            }
                                        }
                                    ]
                                }}
                            />
                        </Listing>

                    </div>
                </div>
            </div>
        </App>
    );
};

NewsView.propTypes = {
    doc: Typings.newsDocument().isRequired
};

export default NewsView;
