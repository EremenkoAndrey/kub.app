import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const RedirectPage = ({ url }) => (
    <div className="redirect-page">
        <div className="redirect-page__block">
            <div className="redirect-page__text">Вы будете перенаправлены на страницу: {url}
            </div>
            <a className="redirect-page__link" href={url}>Нажмите сюда, если редирект не происходит автоматически</a>
        </div>
    </div>
);

RedirectPage.propTypes = {
    url: PropTypes.string.isRequired
};

RedirectPage.defaultProps = {};

export default RedirectPage;
