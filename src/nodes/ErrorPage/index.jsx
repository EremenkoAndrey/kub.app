import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import App from 'layouts/App/';
import DecorativeTitle from 'components/DecorativeTitle';
import Button from 'components/Button/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const ErrorPage = ({ status, text, buttonText }) => (
    <App mods={['bg_pattern']}>
        <div className="error-page">

            <Helmet>
                <title>Страница не найдена - Куб</title>
                <meta name="description" content="Страница не найдена - Куб" />
            </Helmet>

            <div className="error-page__head">
                <DecorativeTitle title={`${status}`} description="ошибка" />
            </div>

            <div className="error-page__text">
                {text[0]}
                <NavLink className="error-page__link" to="/">
                    {text[1]}
                </NavLink>
                {text[2]}
                <NavLink className="error-page__link" to="/">
                    {text[3]}
                </NavLink>
            </div>

            <div className="error-page__button">
                <Button
                    mods={['transparent', 'text_white', 'border_yellow', 'arrows_yellow']}
                    href="/"
                >
                    {buttonText}
                </Button>
            </div>
        </div>
    </App>
);

ErrorPage.propTypes = {
    status: PropTypes.number,
    text: PropTypes.arrayOf(PropTypes.string),
    buttonText: PropTypes.string
};

ErrorPage.defaultProps = {
    status: 404,
    text: [
        'Упс! Такой страницы у нас нет. Но зато у нас есть ',
        'интересные новости ',
        ' и удобная навигация по ним с ',
        'главной страницы'
    ],
    buttonText: 'На главную'
};

export default ErrorPage;
