import React from 'react';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'modules/Config';
import Widget from 'functional/Widget';
import Listing from 'functional/Listing';
import ScrollPageToTop from 'functional/ScrollPageToTop';
import App from 'layouts/App/';
import PromoBox from 'components/PromoBox/';
import TagsMenu from 'components/TagsMenu/';
import NewsCards from 'components/NewsCards/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import Popular from './chunks/Popular';
import './index.less';

const Index = () => {
    const baseHref = `${Config.get('app.protocol')}://${Config.get('app.domain')}`;
    const meta = {
        title: 'КУБ',
        description: 'КУБ',
        image: `${baseHref}/cube_share.png`
    };

    return (
        <App>
            <div className="index-page">
                <ScrollPageToTop />
                <Helmet>
                    <title>{meta.title}</title>
                    <meta name="description" content={meta.description} />

                    <meta property="og:site_name" content={meta.title} />
                    <meta property="og:locale" content="ru_RU" />

                    <meta property="og:type" content="website" />
                    <meta property="og:title" content={meta.title} />
                    <meta property="og:url" content={baseHref} />
                    <meta property="og:description" content={meta.description} />
                    <meta property="og:image" content={meta.image} />

                    <meta property="og:image:type" content="image/jpeg" />
                    <meta property="og:image:width" content="840" />
                    <meta property="og:image:height" content="450" />

                    <meta name="twitter:site" content="@Kub" />
                    <meta name="twitter:card" content="summary_large_image" />

                    <meta name="twitter:title" content={meta.title} />
                    <meta name="twitter:image" content={meta.image} />
                </Helmet>

                <div className="index-page__promo-box">
                    <Widget name="main-promobox">
                        <PromoBox />
                    </Widget>
                </div>

                <div className="index-page__tags-menu">
                    <Widget name="main-menu">
                        <TagsMenu />
                    </Widget>
                </div>

                <div className="index-page__content">
                    <div className="index-page__wrapper">
                        <div className="index-page__columns">
                            <div className="index-page__main">
                                <div className="index-page__news">
                                    <Listing
                                        name="type.Content"
                                        excludeWidgets={['main-promobox']}
                                    >
                                        <NewsCards mods={['index-page']} />
                                    </Listing>
                                </div>
                            </div>
                            <div className="index-page__aside">
                                <Widget name="popular-news">
                                    <Popular />
                                </Widget>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </App>
    );
};

export default Index;
