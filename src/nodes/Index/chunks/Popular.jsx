import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Title from 'components/Title/';
import NumberedLinks from 'components/NumberedLinks/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const Popular = ({ title, docs }) => (
    <div className="index-page__popular">
        <div className="index-page__aside-head">
            <Title>{title}</Title>
        </div>
        <NumberedLinks docs={docs} />
    </div>
);

Popular.propTypes = {
    docs: PropTypes.arrayOf(PropTypes.string),
    title: PropTypes.string
};

Popular.defaultProps = {
    docs: [],
    title: 'Главное на Кубе'
};

export default Popular;
