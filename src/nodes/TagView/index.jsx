import React from 'react';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'modules/Config';
import Typings from 'utils/Typings';
import App from 'layouts/App/';
import Title from 'components/Title/';
import Listing from 'functional/Listing';
import ScrollPageToTop from 'functional/ScrollPageToTop';
import NewsCards from 'components/NewsCards/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';


const TagView = ({ doc }) => {
    const baseHref = `${Config.get('app.protocol')}://${Config.get('app.domain')}`;
    const meta = {
        title: doc.title || doc.metaTitle,
        description: doc.metaDescription || `${doc.title}. Читайте самые последние и интересные новости и материалы по этой теме на сайте Куб.`,
        image: `${baseHref}/cube_share.png`,
        url: `${baseHref}${doc.href}`
    };

    return (
        <App>
            <div className="tag-view">
                <ScrollPageToTop />
                <Helmet>
                    <title>{meta.title} - Куб</title>
                    <meta name="description" content={meta.description} />

                    <meta property="og:site_name" content={meta.title} />
                    <meta property="og:locale" content="ru_RU" />

                    <meta property="og:title" content={meta.title} />
                    <meta property="og:url" content={meta.url} />
                    <meta property="og:description" content={meta.description} />
                    <meta property="og:image" content={meta.image} />

                    <meta property="og:image:type" content="image/jpeg" />
                    <meta property="og:image:width" content="840" />
                    <meta property="og:image:height" content="450" />

                    <meta name="twitter:site" content="@Kub" />
                    <meta name="twitter:card" content="summary_large_image" />

                    <meta name="twitter:title" content={meta.title} />
                    <meta name="twitter:image" content={meta.image} />
                </Helmet>

                <div className="tag-view__content">
                    <div className="tag-view__wrapper">

                        <div className="tag-view__head">
                            <Title tagName="h1" elem="tag-view__title">{`#${doc.title}`}</Title>
                        </div>

                        <Listing
                            name={`type.Content.tag.${doc._id}`}
                            quantity={12}
                        >
                            <NewsCards />
                        </Listing>

                    </div>
                </div>

            </div>
        </App>
    );
};

TagView.propTypes = {
    doc: Typings.tagDocument().isRequired
};

export default TagView;
