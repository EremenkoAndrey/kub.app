import React from 'react';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import App from 'layouts/App/';
import TagsGrid from 'components/TagsGrid';
import Listing from 'functional/Listing';
import ScrollPageToTop from 'functional/ScrollPageToTop';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';


const Tags = () => (
    <App>
        <div className="tags">
            <ScrollPageToTop />
            <Helmet>
                <title>Темы и тренды - КУБ</title>
            </Helmet>

            <div className="tags__content">
                <div className="tags__wrapper">
                    <Listing
                        name="type.Tag"
                        quantity={10}
                    >
                        <TagsGrid />
                    </Listing>
                </div>
            </div>

        </div>
    </App>
);

export default Tags;
