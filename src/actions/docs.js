/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import Config from 'config/';
import API from 'modules/API';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies, import/first */

export function ADD_DOCS(docs) {
    return dispatch => dispatch({ type: 'ADD_DOCS', payload: { docs } });
}

export function FETCH_DOCUMENT(params) {
    return (dispatch) => {
        const _id = `/${params.join('/')}/`;
        API.get('document', [...params], Config.get('api.useAbsPathsForClientRequests'))
            .then(
                doc => dispatch(ADD_DOCS({ [_id]: doc })),
                err => dispatch(ADD_DOCS({ [_id]: err }))
            );

        return dispatch({ type: 'FETCH_DOCUMENT' });
    };
}
