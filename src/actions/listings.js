/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import API from 'modules/API';
import { ADD_DOCS } from 'actions/docs';
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies, import/first */


export function FETCH_LISTING_SUCCESSFUL(listing) {
    return dispatch => dispatch({ type: 'FETCH_LISTING_SUCCESSFUL', payload: { listing } });
}

export function FETCH_LISTING_ERROR(id) {
    return dispatch => dispatch({ type: 'FETCH_LISTING_ERROR', payload: { id } });
}

export function FETCH_LISTING(id, quantity) {
    if (process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line
        console.log('FETCH_LISTING', id);
    }
    return (dispatch, getState) => {
        // Определяем нужную страницу. Она может быть разная в зависимости от
        // конкретного вызова листинга, где-то по 5 элементов запрос, где-то по 12...
        let nextPage = 0;
        const listing = getState().listings[id];

        if (listing) {
            nextPage = (listing.docs.length / quantity).toFixed();
        }

        API.get('listing', [id, quantity, nextPage], Config.get('api.useAbsPathsForClientRequests'))
            .then((data) => {
                dispatch(ADD_DOCS(data.docs));
                dispatch(FETCH_LISTING_SUCCESSFUL({
                    id,
                    ...data.listing
                }));
            })
            .catch(() => dispatch(FETCH_LISTING_ERROR(id)));

        return dispatch({ type: 'FETCH_LISTING' });
    };
}
