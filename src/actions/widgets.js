/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import API from 'modules/API';
import { ADD_DOCS } from 'actions/docs';
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies, import/first */

export function FETCH_WIDGET_SUCCESSFUL(widget) {
    return dispatch => dispatch({ type: 'FETCH_WIDGET_SUCCESSFUL', payload: { widget } });
}

export function FETCH_WIDGET_ERROR(id) {
    return dispatch => dispatch({ type: 'FETCH_WIDGET_ERROR', payload: { id } });
}

export function FETCH_WIDGET(id) {
    if (process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line
        console.log('FETCH_WIDGET', id);
    }
    return (dispatch) => {
        API.get('widget', [id], Config.get('api.useAbsPathsForClientRequests'))
            .then((data) => {
                dispatch(ADD_DOCS(data.docs));
                dispatch(FETCH_WIDGET_SUCCESSFUL({
                    [id]: data.widget
                }));
            })
            .catch(() => dispatch(FETCH_WIDGET_ERROR(id)));

        return dispatch({ type: 'FETCH_WIDGET' });
    };
}
