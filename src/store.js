import { createStore } from 'redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import reducer from 'reducers/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

export default function configureStore(initialState, enhancer) {
    return createStore(
        reducer,
        initialState,
        enhancer
    );
}
