/*
Принимает три необязательных параметра:
- имя блока
- название элемента
- массив модификаторов
возвращает строку с CSS классами, где к названию блока или элмента (но блок в приоритете)
добавлены модификаторы.

Примеры:
    bemNaming('card', '', ['black']) // 'card card_black';
    bemNaming('', 'card__item', ['black', 'long']) // 'card__item_black card__item_long'
 */

const bemNaming = (block = '', element = '', modificators = []) => `
    ${block}
    ${element || ''}
    ${modificators.length ? `${block || element}_${modificators.join(` ${block || element}_`)}` : ''}
    `;

export default bemNaming;
