export default function addScriptToPage(src) {
    const scriptTag = document.createElement('script');
    scriptTag.src = src;
    scriptTag.type = 'text/javascript';
    scriptTag.async = true;
    document.body.appendChild(scriptTag);
    return new Promise((resolve, reject) => {
        scriptTag.onload = () => resolve();
        scriptTag.onerror = () => reject();
    });
}
