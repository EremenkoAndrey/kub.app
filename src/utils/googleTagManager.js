/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import addScriptToPage from 'utils/addScriptToPage';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const id = 'UA-115882547-1';

function gtag() {
    window.dataLayer = window.dataLayer || [];
    /* eslint-disable-next-line */
    window.dataLayer.push(arguments);
}

export function initGTM() {
    addScriptToPage(`//www.googletagmanager.com/gtag/js?id=${id}`);

    gtag('js', new Date());
    gtag('config', id);
}

export function pushToGTM(location) {
    gtag('config', id, { page_path: location.pathname });
}
