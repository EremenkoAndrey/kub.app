import PropTypes from 'prop-types';

export default class Typings {
    static widget() {
        return PropTypes.shape({
            title: PropTypes.string,
            titleLink: PropTypes.string,
            docs: PropTypes.arrayOf(PropTypes.string)
        });
    }
    static newsDocument() {
        return PropTypes.shape({
            _id: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
            summary: PropTypes.string.isRequired,
            href: PropTypes.string.isRequired,
            slug: PropTypes.string.isRequired,
            updatedAt: PropTypes.string.isRequired,
            publishedAt: PropTypes.string.isRequired,
            metaTitle: PropTypes.string.isRequired,
            metaDescription: PropTypes.string.isRequired,
            redirect: this.redirectDocument(),
            tags: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.string.isRequired,
                title: PropTypes.string.isRequired,
                slug: PropTypes.string.isRequired,
                href: PropTypes.string.isRequired
            })).isRequired,
            media: PropTypes.arrayOf(this.mediaObject())
        });
    }
    static bannerDocument() {
        return PropTypes.shape({
            _id: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
            summary: PropTypes.string.isRequired,
            href: PropTypes.string.isRequired,
            slug: PropTypes.string.isRequired,
            updatedAt: PropTypes.string.isRequired,
            publishedAt: PropTypes.string.isRequired,
            openLinkInNewWindow: PropTypes.bool.isRequired,
            media: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.string.isRequired,
                type: PropTypes.string.isRequired,
                source: PropTypes.string.isRequired,
                copyright: PropTypes.string.isRequired,
                title: PropTypes.string.isRequired,
                main: PropTypes.bool.isRequired,
                body: PropTypes.shape({
                    raw: PropTypes.string,
                    original: PropTypes.string.isRequired
                })
            }))
        });
    }

    static tagDocument() {
        return PropTypes.shape({
            _id: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
            summary: PropTypes.string.isRequired,
            href: PropTypes.string.isRequired,
            slug: PropTypes.string.isRequired,
            updatedAt: PropTypes.string.isRequired,
            publishedAt: PropTypes.string.isRequired,
            metaTitle: PropTypes.string.isRequired,
            metaDescription: PropTypes.string.isRequired,
            redirect: this.redirectDocument()
        });
    }

    static redirectDocument() {
        return PropTypes.shape({
            type: PropTypes.string.isRequired,
            to: PropTypes.string.isRequired,
            code: PropTypes.number.isRequired
        });
    }

    static errorDocument() {
        return PropTypes.shape({
            type: PropTypes.string.isRequired,
            message: PropTypes.string.isRequired,
            status: PropTypes.number.isRequired
        });
    }

    static mediaObject() {
        return PropTypes.shape({
            id: PropTypes.string.isRequired,
            type: PropTypes.string.isRequired,
            source: PropTypes.string.isRequired,
            copyright: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
            main: PropTypes.bool.isRequired,
            body: PropTypes.shape({
                raw: PropTypes.string,
                article: PropTypes.string,
                thumbnail: PropTypes.string,
                crop: PropTypes.string,
                original: PropTypes.string.isRequired
            })
        });
    }
}
