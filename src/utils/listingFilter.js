// Фильтр элементов, которые есть в виджетах, переданных в свойстве filterDocsFromWidgets
export default function filter(arr, widgets) {
    if (!arr || !Array.isArray(arr)) {
        return [];
    }
    const widgetsNames = Object.keys(widgets);
    if (!widgetsNames.length) {
        return arr;
    }

    return arr
        .filter(id => !widgetsNames
            .some((widgetName) => {
                if (!widgets[widgetName]) {
                    return false;
                }
                return widgets[widgetName].includes(id);
            }));
};

export
