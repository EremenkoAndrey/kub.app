/*
Функция дополняет переданную строку (или другой тип,
который будет преобразован в строку)
нулями до указанной во втором аргументе длины
 */
export default function zfill(string, width = 0) {
    const _string = `${string}`;
    if (_string.length < width) {
        return `${_string}${'0'.repeat(width - _string.length)}`;
    }
    return _string;
}
