export default (state = {}, action) => {
    switch (action.type) {
    case 'ADD_DOCS':
        return {
            ...state,
            ...action.payload.docs
        };
    default:
        return state;
    }
};
