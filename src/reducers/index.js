import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import docs from './docs';
import listings from './listings';
import widgets from './widgets';
import device from './device';

export default combineReducers({
    routerReducer,
    docs,
    listings,
    widgets,
    device
});
