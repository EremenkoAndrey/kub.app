export default (state = {}, action) => {
    switch (action.type) {
    case 'FETCH_LISTING_SUCCESSFUL': {
        const {
            id, total, nextPage, docs
        } = action.payload.listing;

        if (!state[id]) {
            return {
                ...state,
                [id]: {
                    total,
                    nextPage,
                    docs
                }
            };
        }
        return {
            ...state,
            [id]: {
                total,
                nextPage,
                docs: Array.from(new Set([...state[id].docs, ...docs])) // Исключить возможные дубли
            }
        };
    }
    case 'FETCH_LISTING_ERROR': {
        const { id } = action.payload.listing;
        return {
            ...state,
            [id]: {
                error: true,
                total: state[id].total || null,
                nextPage: state[id].nextPage || '',
                docs: state[id].docs || []
            }
        };
    }
    default:
        return state;
    }
};
