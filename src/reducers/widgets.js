export default (state = {}, action) => {
    switch (action.type) {
    case 'FETCH_WIDGET_SUCCESSFUL':
        return {
            ...state,
            ...action.payload.widget
        };
    default:
        return state;
    }
};
