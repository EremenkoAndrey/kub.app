import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Typings from 'utils/Typings';
import TagsList from 'components/TagsList/';
import Title from 'components/Title/';
import ShareButtons from 'components/ShareButtons/';
import UserContent from 'components/UserContent/';
import Hypercomments from 'components/Hypercomments/';
import TimeFormatted from 'functional/TimeFormatted';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import ArticleCover from './chunks/ArticleCover';
import './index.less';


const Article = ({ doc, messages }) => (
    <section className="article">

        {doc.tags.length ? (
            <div className="article__tags">
                <TagsList tags={doc.tags} mods={['article']} rootLink="/tags/" />
            </div>
        ) : null}

        <div className="article__head">
            <Title tagName="h1" elem="article__title">{doc.title}</Title>

            <time className="article__date">
                {messages.published} <TimeFormatted ms={doc.publishedAt} />
            </time>
        </div>

        <ArticleCover media={doc.media} title={doc.title} />

        <div className="article__share-buttons">
            <ShareButtons
                buttons={['vk', 'facebook', 'twitter', 'odnoklassniki', 'telegram', 'fbmessenger', 'whatsapp']}
                pageTitle={doc.title}
                href={doc.href}
                tags={doc.tags.map(tag => tag.title)}
            />
        </div>

        {doc.summary ? <div className="article__summary">{doc.summary}</div> : null}

        <UserContent elem="article__content" content={doc.text} media={doc.media} />

        <Hypercomments xid={doc.id} />
    </section>
);

Article.propTypes = {
    doc: Typings.newsDocument().isRequired,
    messages: PropTypes.shape({
        published: PropTypes.string
    })
};

Article.defaultProps = {
    messages: {
        published: 'Дата публикации:'
    }
};

export default Article;
