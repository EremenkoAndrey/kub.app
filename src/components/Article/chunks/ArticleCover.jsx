import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import ImgCaption from 'components/ImgCaption/';
import Video from 'components/Video/';
import Typings from 'utils/Typings';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const ArticleCover = ({ media, title }) => {
    const image = media.find(mediaElem => mediaElem.type.toLowerCase() === 'cover' && mediaElem.main);
    const video = media.find(mediaElem => mediaElem.type.toLowerCase() === 'externalvideo' && mediaElem.main);

    if (video) {
        return (
            <div className="article__video">
                <Video video={video} />
            </div>
        );
    } else if (image) {
        return (
            <div className="article__cover">
                <img className="article__image" src={image.body.article} alt={image.title || title} />
                <div className="article__caption">
                    <ImgCaption mediaElement={image} />
                </div>
            </div>);
    }
    return null;
};

ArticleCover.propTypes = {
    media: PropTypes.arrayOf(Typings.mediaObject()),
    title: PropTypes.string
};

ArticleCover.defaultProps = {
    media: null,
    title: ''
};

export default ArticleCover;
