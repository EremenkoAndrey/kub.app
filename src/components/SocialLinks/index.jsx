import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const SocialLinks = props => (
    <div className="social-links">
        <ul className="social-links__list">
            {props.items.map(item => (
                <li key={item.link} className="social-links__item">
                    <a
                        target="_blank"
                        href={item.link}
                        className={`social-links__link social-links__link_${item.name.toLowerCase()}`}
                    >
                        {item.name}
                    </a>
                </li>
            ))}
        </ul>
    </div>

);


SocialLinks.propTypes = {
    // Элементы
    items: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        link: PropTypes.string
    }))
};

SocialLinks.defaultProps = {
    items: [
        {
            name: 'Youtube',
            link: 'https://www.youtube.com/channel/UCpAuewi6cXxqq8EACa0ULKg/featured?view_as=subscriber'
        },
        {
            name: 'VK',
            link: 'https://vk.com/kub_media'
        },
        {
            name: 'Instagram',
            link: 'https://www.instagram.com/kub_media/'
        },
        {
            name: 'Facebook',
            link: 'https://www.facebook.com/Kybmedia/'
        },
        {
            name: 'Telegram',
            link: 'https://t.me/kub_media'
        },
        {
            name: 'Twitter',
            link: 'https://twitter.com/Kub_media'
        },
        {
            name: 'OK',
            link: 'https://ok.ru/group/55120693690468'
        }
    ]
};

export default SocialLinks;
