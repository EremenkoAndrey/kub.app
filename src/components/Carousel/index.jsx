import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import './index.less';

/*
* Обертка над компонентом react-slick.
* Принимаемые параметры:
*  - docs: массив из строковых ID документов
*  - settings: настройки карусели, полный список см. в документации:
*  https://react-slick.neostack.com/docs/api
*  - view - функция, stateless component, которая получит в аргументе
*  свойство doc (доступно как props.doc) -
*  подписанное на изменения соответствующего объекта в store
*  Любые другие свойства, переданные компоненту Carousel, будут проксированы
*  в компонент, переданный свойстве view
 */

const mapStateToProps = (state, ownProps) => (
    {
        doc: state.docs[ownProps.id]
    }
);

const SlideElement = connect(mapStateToProps)(({
    component: Component, doc, ...rest
}) => <Component doc={doc} {...rest} />);


const Carousel = ({
    docs, settings, view, ...rest
}) => (
    <Slider {...settings}>
        {docs.map(docId => (
            <div key={docId} className="slick-slide__element-container">
                <SlideElement key={docId} component={view} id={docId} {...rest} />
            </div>
        ))}
    </Slider>
);

Carousel.propTypes = {
    docs: PropTypes.arrayOf(PropTypes.string),
    view: PropTypes.func.isRequired,
    // eslint-disable-next-line
    settings: PropTypes.object
};

Carousel.defaultProps = {
    docs: [],
    settings: {}
};

export default Carousel;
