import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Typings from 'utils/Typings';
import Title from 'components/Title/';
import Button from 'components/Button/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const card = (doc, viewParams) => {
    const cover = doc.media.find(mediaElem => mediaElem.type.toLowerCase() === 'cover' && mediaElem.main);
    const video = doc.media.find(mediaElem => mediaElem.type.toLowerCase() === 'externalvideo' && mediaElem.main);

    return (
        <React.Fragment>
            <NavLink
                to={doc.href}
                className="promo-box-card__cover"
                style={{ backgroundImage: `url(${cover ? cover.body.article : ''})` }}
            >
                {doc.title}

                {viewParams.coverDecorations ?
                    viewParams.coverDecorations.map(decorator => (
                        <span key={decorator} className={`promo-box-card__decorator promo-box-card__decorator_${decorator}`} />)) :
                    null}
            </NavLink>

            <div
                className={`promo-box-card__description
                ${viewParams.backgroundColor ? `promo-box-card__description_background_${viewParams.backgroundColor}` : ''}
                `}
            >

                <Title elem="promo-box-card__title">
                    <NavLink
                        to={doc.href}
                        className={`promo-box-card__link
                            ${viewParams.color ? `promo-box-card__link_color_${viewParams.color}` : ''}`}
                    >
                        {doc.title}
                    </NavLink>
                </Title>

                {viewParams.size === 'm' ? (
                    <React.Fragment>
                        <div className="promo-box-card__summary">
                            {doc.summary}
                        </div>
                        <Button
                            mods={['transparent', 'text_white', 'border_yellow', 'arrows_yellow']}
                            elem="promo-box-card__button"
                            href={doc.href}
                        >
                            {video ? 'Смотреть' : 'Читать'}
                        </Button>
                    </React.Fragment>
                ) : null}

            </div>
        </React.Fragment>
    );
};

const banner = (doc) => {
    const cover = doc.media.find(mediaElem => mediaElem.type.toLowerCase() === 'cover' && mediaElem.main);
    const className = 'promo-box-card__banner';

    if (doc.href[0] === '/') {
        return (
            <NavLink
                className={className}
                href={doc.href}
                title={doc.title}
                style={{ backgroundImage: `url(${cover.body.original})` }}
                target={doc.openLinkInNewWindow ? '_blank' : ''}
            >
                {doc.title}
            </NavLink>
        );
    }
    return (
        <a
            className={className}
            href={doc.href}
            title={doc.title}
            style={{ backgroundImage: `url(${cover.body.original})` }}
            target={doc.openLinkInNewWindow ? '_blank' : ''}
        >
            {doc.title}
        </a>
    );
};


const PromoBoxCard = ({ viewParams, doc, id }) => {
    if (!doc) {
        return null;
    }
    return (
        <div className={`promo-box-card
              ${viewParams.size ? `promo-box-card_size_${viewParams.size}` : ''}
              ${viewParams.direction ? `promo-box-card_direction_${viewParams.direction}` : ''}
        `}
        >
            {(doc.type === 'News') ?
                card(doc, viewParams, id) :
                banner(doc)}

        </div>
    );
};

PromoBoxCard.propTypes = {
    id: PropTypes.string.isRequired,
    viewParams: PropTypes.shape({
        size: PropTypes.string,
        direction: PropTypes.string
    }).isRequired,
    doc: PropTypes.oneOfType([
        Typings.newsDocument(),
        Typings.bannerDocument()
    ])
};

PromoBoxCard.defaultProps = {
    doc: null
};

const mapStateToProps = (state, ownProps) => ({ doc: state.docs[ownProps.id] });

export default connect(mapStateToProps)(PromoBoxCard);
