import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'modules/Config';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

export default class Hypercomments extends React.Component {
    constructor(props) {
        super(props);

        this.config = {
            userHC_id: Config.get('app.vendors.hypercomments.id')
        };
    }

    componentDidMount() {
        if ('HC_LOAD_INIT' in window) {
            window.HC.widget('Stream', {
                widget_id: this.config.userHC_id,
                xid: this.props.xid
            });
            return;
        }

        window._hcwp = window._hcwp || [];
        window._hcwp.push({
            widget: 'Stream',
            widget_id: this.config.userHC_id,
            xid: this.props.xid
        });

        window.HC_LOAD_INIT = true;

        const lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || 'en').substr(0, 2).toLowerCase();

        const hcc = document.createElement('script'); hcc.type = 'text/javascript';
        hcc.async = true;

        hcc.src = `${document.location.protocol === 'https:' ? 'https' : 'http'}://w.hypercomments.com/widget/hc/104487/${lang}/widget.js`;
        const s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(hcc, s.nextSibling);
    }

    componentDidUpdate(prevProps) {
        if (!window || !window.HC || !this.props.xid) {
            return;
        }
        if (this.props.xid !== prevProps.xid) {
            window.HC.widget('Stream', {
                widget_id: this.config.userHC_id,
                xid: this.props.xid
            });
        }
    }

    render() {
        return (
            <div id="hypercomments_widget" />
        );
    }
}

Hypercomments.propTypes = {
    xid: PropTypes.string.isRequired
};
