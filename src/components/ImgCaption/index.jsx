import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import bemNaming from 'utils/bemNaming';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const ImgCaption = ({
    mediaElement, bemNames, elem, mods
}) => {
    const source = mediaElement.source && mediaElement.source !== 'Free' ? mediaElement.source : null;
    const { copyright } = mediaElement;

    return (source || copyright ? (
        <div
            className={bemNames('img-caption', elem, mods)}
        >
            <ul className={bemNames('', 'img-caption__list', mods)}>
                {copyright ? (
                    <li className={bemNames('', 'img-caption__item', [...mods, 'copyright'])}>
                        {copyright}
                    </li>) : null}
                {source ? (
                    <li className={bemNames('', 'img-caption__item', [...mods, 'source'])}>
                        {source}
                    </li>) : null}
            </ul>
        </div>
    ) : null);
};

ImgCaption.propTypes = {
    mediaElement: PropTypes.shape({
        source: PropTypes.string.isRequired,
        copyright: PropTypes.string.isRequired
    }).isRequired,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    bemNames: PropTypes.func
};

ImgCaption.defaultProps = {
    mods: [],
    elem: '',
    bemNames: bemNaming
};

export default ImgCaption;
