import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import './index.less';

const Button = ({
    onClick, children, href, elem, mods
}) => {
    const classNames = `button ${elem || ''} ${mods.length ? ` button_${mods.join(' button_')}` : ''}`;
    if (href) {
        return (
            <NavLink
                to={href}
                className={classNames}
            >
                {children}
            </NavLink>
        );
    }
    return (
        <button
            onClick={onClick}
            className={classNames}
        >
            {children}
        </button>
    );
};

Button.propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]),
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    href: PropTypes.string
};

Button.defaultProps = {
    onClick: null,
    children: null,
    mods: [],
    elem: '',
    href: null
};

export default Button;
