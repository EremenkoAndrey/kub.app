import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import Config from 'config/';
import API from 'modules/API';
import Adapter from 'modules/Adapter';
import Title from 'components/Title/';
import Card from 'components/Card/';
import Button from 'components/Button/';
import Loader from 'components/Loader/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import './index.less';

export default class SearchResults extends React.Component {
    static getSuffix(number) {
        let end = '';
        // 2, 3, 4
        if (number > 1 && number < 5) {
            end = 'a';

            // 5 - 19 и 20, 30, 40... 100, 155
        } else if (((number > 4) && (number < 20)) ||
            ((number % 10) === 0) ||
            ((number % 10) > 4 && (number % 10) < 10)) {
            end = 'ов';

            // 22, 23, 32, 44 ...
        } else if ((number % 10) > 1 && (number % 10) < 5) {
            end = 'a';
        }
        return end;
    }

    static formatResultItems(items) {
        if (!Array.isArray(items) || !items.length) {
            return [];
        }
        return items.map(item => Adapter.toNewsDocument(item));
    }

    constructor(props) {
        super(props);
        this._page = 0;
        this._loading = false;

        this.state = {
            status: '',
            title: props.queryString,
            found: null,
            hasNext: false,
            results: []
        };
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.queryString !== prevState.title) {
            return {
                title: nextProps.queryString,
                found: null,
                hasNext: false,
                results: []
            };
        }
        return {};
    }

    componentDidMount() {
        this.fetchResults();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.title !== prevState.title) {
            this._page = 0;
            this.fetchResults();
        }
    }

    componentWillUnmount() {
        clearTimeout(this._timer);
    }

    onClick = () => {
        if (!this.state.hasNext) {
            return;
        }
        this.fetchResults();
    };

    fetchResults() {
        if (this._loading) {
            return;
        }
        this._page += 1;
        this._loading = true;
        this._timer = setTimeout(() => {
            if (!this._loading) {
                return;
            }

            this.setState({
                status: 'loading'
            });
        }, 200);

        API.get('search', { q: this.state.title, page: this._page }, Config.get('api.useAbsPathsForClientRequests'))
            .then(({ data }) => {
                const safeItems = SearchResults.formatResultItems(data.items);

                this.setState({
                    found: data.total,
                    hasNext: !!data.nextPage,
                    results: this.state.results.concat(safeItems)
                }, () => {
                    this._loading = false;
                    clearTimeout(this._timer);
                });
            });
    }

    render() {
        const {
            title, found, status, hasNext
        } = this.state;

        const summary = () => {
            const text = this.state.found ?
                `Мы нашли ${this.state.found} результат${SearchResults.getSuffix(this.state.found)}` :
                'К сожалению ничего не найдено, попробуйте изменить или уточнить запрос';
            return (<Title elem="search-results__summary" mods={['slim']}>{text}</Title>);
        };

        const meta = {
            title: title ? `${title} - найдено ${found} результатов` : 'Найти на КУБе',
            description: found ?
                `Результаты поиска: по фразе ${title} найдено ${found} материалов` :
                'Результаты поиска'
        };


        return (
            <div className="search-results">

                <Helmet>
                    <title>{meta.title}</title>
                    <meta name="description" content={meta.description} />
                </Helmet>

                <div className="search-results__head">
                    {this.state.title ?
                        <Title tagName="h1" elem="search-results__title">{`«${this.state.title}»`}</Title> :
                        <Title tagName="h1" elem="search-results__title">Задан пустой поисковый запрос</Title>
                    }

                    {this.state.title && this.state.found !== null ? summary() : null }
                </div>

                <div className="search-results__list">
                    {this.state.results.map(item => (
                        <div key={item.id} className="search-results__item"><Card doc={item} mods={['max_height']} /></div>
                    ))}
                </div>

                <div className="search-results__actions">
                    <div className="search-results__action">
                        {status === '' && hasNext ? (
                            <Button mods={['big', 'transparent']} onClick={this.onClick}>Загрузить еще</Button>
                        ) : null}
                        {status === 'loading' && (
                            <Loader />
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

SearchResults.propTypes = {
    queryString: PropTypes.string
};

SearchResults.defaultProps = {
    queryString: ''
};
