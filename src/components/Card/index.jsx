import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Title from 'components/Title/';
import TagsList from 'components/TagsList/';
import TimeFormatted from 'functional/TimeFormatted';
import Typings from 'utils/Typings';
import bemNaming from 'utils/bemNaming';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const Card = ({
    elem, mods: incomingMods, doc, composition, bemNames
}) => {
    const cover = doc.media.find(mediaElem => mediaElem.type.toLowerCase() === 'cover' && mediaElem.main);
    const mods = [...incomingMods];
    // Установлены дефолтные значения
    const _composition = {
        date: true,
        summary: true,
        tags: true,
        ...composition
    };

    // Пометить модификатором материалы с тегом Новости
    if (doc.tags.some(tag => tag.id === '5afd4ec439a64d41d8450836')) {
        mods.push('colored');
    }
    return (
        <div className={bemNames('card', elem, mods)}>
            <div className="card__grid">
                {cover ? (
                    <NavLink className={bemNames('', 'card__cover', mods)} to={doc.href}>
                        <img className="card__img" src={cover.body.thumbnail} alt={cover.body.title || doc.title} />
                    </NavLink>
                ) : null}
                <div className="card__content">

                    <Title elem="card__title">
                        <NavLink to={doc.href} className={bemNames('', 'card__link', mods)}>{doc.title}</NavLink>
                    </Title>

                    {_composition.date && doc.publishedAt ? (
                        <div className={bemNames('', 'card__date', mods)}>
                            <TimeFormatted ms={doc.publishedAt} />
                        </div>
                    ) : null}

                    {_composition.summary && doc.summary ? (
                        <div className={bemNames('', 'card__summary', mods)}>
                            {doc.summary}
                        </div>
                    ) : null}

                </div>

                {_composition.tags && doc.tags.length ? (
                    <div className={bemNames('', 'card__signature', mods)}>
                        <div className="card__tags">
                            <TagsList tags={doc.tags} />
                        </div>
                    </div>
                ) : null}
            </div>
        </div>
    );
};

Card.propTypes = {
    doc: Typings.newsDocument().isRequired,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    // свойство composition для управления показом элементов
    composition: PropTypes.shape({
        date: PropTypes.bool,
        summary: PropTypes.bool,
        tags: PropTypes.bool
    }),
    bemNames: PropTypes.func
};

Card.defaultProps = {
    mods: ['ddd'],
    elem: '',
    composition: {},
    bemNames: bemNaming
};

export default Card;
