import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import SocialLinks from 'components/SocialLinks/';
import SearchForm from 'components/SearchForm/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const MainMenu = props => (
    <div
        className={`main-menu
        ${props.elem ? props.elem : ''}
        `}
    >
        {props.items.map(item => (
            <NavLink
                key={item.link}
                to={item.link}
                className="main-menu__home-link"
                title={item.title}
            >
                {item.title}
            </NavLink>
        ))}

        <div className="main-menu__search">
            <SearchForm />
        </div>

        <div className="main-menu__social-links">
            <SocialLinks />
        </div>
    </div>
);

MainMenu.propTypes = {
    // Элементы меню
    items: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string,
        link: PropTypes.string
    })),
    // CSS-элемент
    elem: PropTypes.string
};

MainMenu.defaultProps = {
    items: [
        {
            title: 'На главную',
            link: '/'
        }
    ],
    elem: ''
};

export default MainMenu;
