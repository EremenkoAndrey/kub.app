import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import bemNaming from 'utils/bemNaming';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const TagsList = ({
    tags, rootLink, elem, mods, bemNames
}) => (
    <ul className={bemNames('tags-list', elem, mods)}>
        {rootLink ? (
            <li className={bemNames('', 'tags-list__item', [...mods, 'root'])}>
                <NavLink
                    className={bemNames('', 'tags-list__link', mods)}
                    to={rootLink}
                >
                #
                </NavLink>
            </li>
        ) : null}

        {tags.map(tag => (
            <li className={bemNames('', 'tags-list__item', mods)} key={tag.id}>
                <NavLink
                    className={bemNames('', 'tags-list__link', mods)}
                    to={tag.href}
                >
                    {tag.title}
                </NavLink>
            </li>
        ))}
    </ul>
);

TagsList.propTypes = {
    tags: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        href: PropTypes.string
    })),
    rootLink: PropTypes.string,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    bemNames: PropTypes.func
};

TagsList.defaultProps = {
    tags: [],
    rootLink: '',
    mods: [],
    elem: '',
    bemNames: bemNaming
};

export default TagsList;
