export default function rtCodeTmpl(data) {
    const code = (data.body && data.body.raw) ? data.body.raw : '';
    if (data.provider.toLowerCase() === 'twitter') {
        return `<div class="user-content__code user-content__code_twitter" id="${data.body.original}"></div>`;
    }
    return `<div class="user-content__code">${code}</div>`;
}
