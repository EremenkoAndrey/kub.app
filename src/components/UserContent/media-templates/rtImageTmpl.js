export default function rtImageTmpl(data) {
    const src = data.body.article || data.body.original;
    const alt = data.title || '';
    const source = data.source !== 'Free' ? data.source : '';
    const caption = `${data.copyright}${source ? ` ${source}` : ''}`;
    if (!src) {
        return '';
    }
    return `<figure class="user-content__img-container">
        <img class="user-content__img" src="${src}" alt="${alt}" />
        <figcaption class="user-content__caption">${alt}${caption ? ` ${caption}` : ''}</figcaption>
    </figure>`;
}
