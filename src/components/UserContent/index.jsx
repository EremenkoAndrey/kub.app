import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import bemNaming from 'utils/bemNaming';
import Typings from 'utils/Typings';
import Content from 'modules/Content';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import TwitterWidgets from './utils/TwitterWidgets';
import rtCodeTmpl from './media-templates/rtCodeTmpl';
import rtImageTmpl from './media-templates/rtImageTmpl';
import './index.less';

export default class UserContent extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        new TwitterWidgets(this.props.media).init()
    }

    componentDidUpdate() {
        new TwitterWidgets(this.props.media).init()
    }
    render() {
        const { mods, elem, media, bemNames } = this.props;
        const _content = new Content(this.props.content, {
            code: rtCodeTmpl,
            image: rtImageTmpl
        });

        return (
            <div
                className={bemNames('user-content', elem, mods)}
                dangerouslySetInnerHTML={{ __html: _content.insertMedia(media) }}
            />
        )
    }

}

UserContent.propTypes = {
    content: PropTypes.string,
    media: PropTypes.arrayOf(Typings.mediaObject()),
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    bemNames: PropTypes.func
};

UserContent.defaultProps = {
    content: '',
    media: [],
    mods: [],
    elem: '',
    bemNames: bemNaming
};
