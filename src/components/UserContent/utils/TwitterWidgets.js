/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import addScriptToPage from 'utils/addScriptToPage';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

export default class TwitterWidgets {
    constructor(mediaElements) {
        this._twitterWidgets = mediaElements.filter(media => media.provider.toLowerCase() === 'twitter');
    }

    _addScriptToPage() {
        addScriptToPage('//platform.twitter.com/widgets.js').then(() => {
            this.init();
        });
    }

    init() {
        if (!this._twitterWidgets.length) {
            return;
        }
        if (!window.twttr) {
            this._addScriptToPage();
            return;
        }

        this._twitterWidgets.forEach((widget) => {
            const widgetId = `${widget.body.original}`;
            const rootElement = document.getElementById(widgetId);
            if (rootElement) {
                window.twttr.widgets.createTweet(
                    widgetId,
                    document.getElementById(widgetId)
                );
            }
        });
    }
}
