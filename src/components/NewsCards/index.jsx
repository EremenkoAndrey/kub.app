import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Card from 'components/Card/';
import Loader from 'components/Loader/';
import Button from 'components/Button/';
import bemNaming from 'utils/bemNaming';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';


const mapStateToProps = (state, ownProps) => (
    {
        doc: state.docs[ownProps.id],
        mods: ['max_height']
    }
);

const NewsCard = connect(mapStateToProps)(Card);

const NewsCards = ({
    docs, status, getMore, buttonText, bemNames, elem, mods
}) => {
    const onClick = () => getMore();
    return (
        <div className={bemNames('news-cards', elem, mods)}>
            <div className={bemNames('', 'news-cards__columns', mods)}>
                {docs.map(id => (
                    <div
                        key={id}
                        className={bemNames('', 'news-cards__item', mods)}
                    >
                        <NewsCard id={id} />
                    </div>
                ))}
            </div>
            <div className={bemNames('', 'news-cards__actions', mods)}>
                <div className={bemNames('', 'news-cards__action', mods)}>
                    {status === '' && (
                        <Button mods={['big', 'transparent']} onClick={onClick}>{buttonText}</Button>
                    )}
                    {status === 'loading' && (
                        <Loader />
                    )}
                </div>
            </div>
        </div>
    );
};

NewsCards.propTypes = {
    getMore: PropTypes.func,
    status: PropTypes.string,
    docs: PropTypes.arrayOf(PropTypes.string),
    buttonText: PropTypes.string,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    bemNames: PropTypes.func
};

NewsCards.defaultProps = {
    getMore: null,
    status: '',
    docs: [],
    buttonText: 'Загрузить еще',
    mods: [],
    elem: '',
    bemNames: bemNaming
};

export default NewsCards;
