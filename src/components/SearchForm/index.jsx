import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import './index.less';


class SearchForm extends React.Component {
    constructor() {
        super();
        this.state = {
            open: false,
            value: ''
        };
    }

    toggle = () => {
        this.setState({
            open: !this.state.open
        });
    };

    updateInputValue = (e) => {
        this.setState({
            value: e.target.value
        });
    };

    goToSearchPage = (e) => {
        if (!this.state.value || e.charCode !== 13) {
            return;
        }
        this.props.history.push(`/search/?q=${this.state.value}`);
    };

    render() {
        return (
            <div className="search-form">
                <button className="search-form__icon" onClick={this.toggle} />
                <div className={`search-form__body${this.state.open ? ' search-form__body_open' : ''}`}>

                    <input
                        className="search-form__input"
                        value={this.state.value}
                        onChange={this.updateInputValue}
                        onKeyPress={this.goToSearchPage}
                    />

                    {this.state.value ? (
                        <NavLink className="search-form__link" to={`/search/?q=${this.state.value}`} />
                    ) : (
                        <span className="search-form__link search-form__link_no_active" />
                    )}
                </div>
            </div>
        );
    }
}

SearchForm.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func
    }).isRequired
};

export default withRouter(SearchForm);
