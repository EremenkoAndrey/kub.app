import React from 'react';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Typings from 'utils/Typings';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const Video = ({ video }) => (
    <div className="video">
        <iframe
            className="video__frame"
            src={`//www.youtube.com/embed/${video.body.original}`}
            title={video.body.original}
            frameBorder="0"
            allowFullScreen
        />
    </div>
);

Video.propTypes = {
    video: Typings.mediaObject().isRequired
};


export default Video;
