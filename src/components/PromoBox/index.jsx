import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import PromoBoxCard from 'components/PromoBoxCard/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import pattern from './pattern';
import './index.less';

const PromoBox = (props) => {
    const getId = (arr) => {
        let _counter = 0;
        return {
            next: () => {
                const element = arr[_counter];
                _counter += 1;
                return element || null;
            }
        };
    };
    const docId = getId(props.docs);

    return (
        <div className="promo-box">
            {props.pattern.map(section => (
                <div
                    key={section.key}
                    className={`promo-box__item
                        ${section.size ? `promo-box__item_size_${section.size}` : ''}
                        ${section.items.length > 1 ? 'promo-box__item_has-two-card' : ''}
                        `}
                >
                    {section.items.map((item) => {
                        const id = docId.next();
                        return <PromoBoxCard key={id} id={id} viewParams={item.viewParams} />;
                    })}
                </div>
            ))}
        </div>
    );
};

PromoBox.propTypes = {
    docs: PropTypes.arrayOf(PropTypes.string),
    pattern: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string,
        size: PropTypes.string,
        items: PropTypes.arrayOf(PropTypes.shape({
            viewParams: PropTypes.shape({
                location: PropTypes.string,
                direction: PropTypes.string,
                size: PropTypes.string,
                backgroundColor: PropTypes.string,
                color: PropTypes.string,
                coverDecorations: PropTypes.arrayOf(PropTypes.string)
            })
        }))
    }))
};

PromoBox.defaultProps = {
    docs: [],
    pattern
};

export default PromoBox;
