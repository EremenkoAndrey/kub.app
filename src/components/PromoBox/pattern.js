const pattern = [
    {
        size: 'one',
        key: 'big-1',
        items: [
            {
                viewParams: {
                    location: 'promo-box',
                    direction: 'row-reverse',
                    coverDecorations: ['blue-hexagon', 'black-spikes'],
                    size: 'm',
                    backgroundColor: 'black',
                    color: 'white'
                }
            }
        ]
    },
    {
        size: 'one',
        key: 'big-2',
        items: [
            {
                viewParams: {
                    location: 'promo-box',
                    direction: 'row-reverse',
                    coverDecorations: ['blue-filter', 'blue-dots'],
                    size: 'm',
                    backgroundColor: 'blue',
                    color: 'white'
                }
            }
        ]
    },
    {
        size: 'one',
        key: 'big-3',
        items: [
            {
                viewParams: {
                    location: 'promo-box',
                    direction: 'row-reverse',
                    coverDecorations: ['blue-hexagon', 'black-spikes'],
                    size: 'm',
                    backgroundColor: 'black',
                    color: 'white'
                }
            }
        ]
    },
    {
        size: 'half',
        key: 'small-1',
        items: [
            {
                viewParams: {
                    location: 'promo-box',
                    direction: 'row-reverse',
                    size: 's',
                    backgroundColor: 'blue',
                    color: 'white'
                }
            },
            {
                viewParams: {
                    location: 'promo-box',
                    type: 'event',
                    direction: 'row',
                    coverDecorations: ['yellow-background', 'yellow-spikes-outside'],
                    size: 's',
                    backgroundColor: 'black',
                    color: 'white'
                }
            }
        ]
    },
    {
        size: 'half',
        key: 'small-2',
        items: [
            {
                viewParams: {
                    location: 'promo-box',
                    direction: 'row',
                    size: 's',
                    backgroundColor: 'white',
                    color: 'black'
                }
            },
            {
                viewParams: {
                    location: 'promo-box',
                    type: 'event',
                    direction: 'row-reverse',
                    coverDecorations: ['blue-spikes'],
                    size: 's',
                    backgroundColor: 'blue',
                    color: 'white'
                }
            }
        ]
    },
    {
        size: 'half',
        key: 'additional',
        items: [
            {
                viewParams: {
                    size: 's'
                }
            }
        ]
    }
];

export default pattern;
