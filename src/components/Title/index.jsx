import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

/*
* Пример использования:
* <Title tagName={'h1'} elem="card__title" mods={['black']}>Заголовок</Title>
 */
const Title = ({
    tagName, children, elem, mods
}) => {
    if (!children) {
        return null;
    }
    return React.createElement(tagName, {
        className: `title ${elem || ''} ${mods.length ? ` title_${mods.join(' title_')}` : ''}`
    }, children);
};

Title.propTypes = {
    tagName: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]),
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string
};

Title.defaultProps = {
    tagName: 'div',
    children: null,
    mods: [],
    elem: ''
};

export default Title;
