import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import './index.less';

const FooterMenu = ({ items }) => (
    <ul className="footer__menu">
        {items.map((item, i, arr) => {
            const isLast = arr.length === i + 1;
            return (
                <li
                    className={`footer__menu-item ${isLast ? 'footer__menu-item_last' : ''}`}
                    key={item.link}
                >
                    {item.link[0] === '/' ? (
                        <NavLink to={item.link} className="footer__menu-link">{item.text}</NavLink>
                    ) : (
                        <a href={item.link} className="footer__menu-link">{item.text}</a>
                    )}

                </li>
            );
        })}
    </ul>
);


const Footer = ({ menuOne, menuTwo, elem }) => (
    <div className={`footer ${elem}`}>
        <div className="footer__menu footer__menu_part_one">
            <FooterMenu items={menuOne} />
        </div>

        <div className="footer__menu footer__menu_part_two">
            <FooterMenu items={menuTwo} />
        </div>
        <div className="footer__age">
            18+
        </div>
    </div>
);

const menuPropTypes = PropTypes.arrayOf(PropTypes.shape({
    link: PropTypes.string,
    text: PropTypes.string
}));

FooterMenu.propTypes = {
    items: menuPropTypes.isRequired
};

Footer.propTypes = {
    menuOne: menuPropTypes,
    menuTwo: menuPropTypes,
    elem: PropTypes.string
};


Footer.defaultProps = {
    elem: '',
    menuOne: [
        {
            text: 'Отравление Скрипалей',
            link: '/tags/otravlenie-skripalei/'
        },
        {
            text: 'MH-17',
            link: '/tags/mh-17/'
        },
        {
            text: 'Санкции',
            link: '/tags/sanctions/'
        }
    ],
    menuTwo: [
        {
            text: 'О проекте',
            link: '/news/-o-proekte/'
        },
        {
            text: 'Программы',
            link: 'https://www.youtube.com/channel/UCpAuewi6cXxqq8EACa0ULKg/playlists'
        },
        {
            text: 'Новости',
            link: '/tags/news/'
        }
    ]
};

export default Footer;

