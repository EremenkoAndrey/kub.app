import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const Loader = props => (
    <div className="loader">
        <svg className="loader__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill={props.color}>
            <path d="M304 48c0 26.51-21.49 48-48
            48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48
            48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48
            48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51
            0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96
            256c0-26.51-21.49-48-48-48S0 229.49 0 256s21.49 48 48 48 48-21.49
            48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49
            48 48 48 48-21.49 48-48c0-26.509-21.491-48-48-48zm294.156
            0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49
            48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48
            21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.491-48-48-48z"
            >
                <animateTransform
                    attributeName="transform"
                    type="rotate"
                    from="0 256 256"
                    to="360 256 256"
                    begin="0s"
                    dur="2s"
                    repeatCount="indefinite"
                />
            </path>
        </svg>
    </div>
);

Loader.propTypes = {
    color: PropTypes.string
};

Loader.defaultProps = {
    color: '#000'
};

export default Loader;
