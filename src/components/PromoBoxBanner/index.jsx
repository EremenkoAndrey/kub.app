import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const PromoBoxBanner = () => (
    <div>PromoBoxBanner</div>
);

PromoBoxBanner.propTypes = {};

PromoBoxBanner.defaultProps = {};

const mapStateToProps = (state, ownProps) => ({ doc: state.docs[ownProps.id] });

export default connect(mapStateToProps)(PromoBoxBanner);
