import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import TimeFormatted from 'functional/TimeFormatted';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';


const mapStateToProps = (state, ownProps) => ({ doc: state.docs[ownProps.id] });

const Document = connect(mapStateToProps)(({ doc }) => (
    <NavLink className="numbered-links__link" to={doc.href}>
        <span className="numbered-links__title">{doc.title}</span>
        <span className="numbered-links__time"><TimeFormatted ms={doc.publishedAt} /></span>
        <span className="numbered-links__summary">{doc.summary}</span>
    </NavLink>
));

const NumberedLinks = ({ docs }) => (
    <ol className="numbered-links">
        {docs.map(docId => (
            <li
                className="numbered-links__item"
                key={docId}
            >
                <Document id={docId} />
            </li>
        ))}
    </ol>
);

NumberedLinks.propTypes = {
    docs: PropTypes.arrayOf(PropTypes.string)
};

NumberedLinks.defaultProps = {
    docs: []
};

export default NumberedLinks;
