import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Masonry from 'components/Masonry';
import Button from 'components/Button/';
import Loader from 'components/Loader/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const mapStateToProps = (state, ownProps) => (
    {
        doc: state.docs[ownProps.id]
    }
);

const TagCard = connect(mapStateToProps)(({ doc }) => (
    <div className="tags-grid__card">
        <NavLink
            className="tags-grid__link"
            to={doc.href}
        >
            {doc.title}
        </NavLink>
    </div>
));

const TagsGrid = ({
    docs, status, getMore, buttonText
}) => {
    const _docs = ['#', ...docs];
    const onClick = () => getMore();
    return (
        <div className="tags-grid">
            <Masonry mods={['randomize', 'tags-grid']}>
                {_docs.map((id) => {
                    if (id === '#') {
                        return (<div className="tags-grid__card tags-grid__card_hash" key={id}>{id}</div>);
                    }
                    return (<TagCard id={id} key={id} />);
                })}
            </Masonry>
            <div className="tags-grid__action">
                {status === '' && (
                    <Button mods={['big', 'transparent']} onClick={onClick}>{buttonText}</Button>
                )}
                {status === 'loading' && (
                    <Loader />
                )}
            </div>
        </div>
    );
};

TagsGrid.propTypes = {
    docs: PropTypes.arrayOf(PropTypes.string),
    getMore: PropTypes.func,
    status: PropTypes.string,
    buttonText: PropTypes.string
};

TagsGrid.defaultProps = {
    docs: [],
    getMore: null,
    status: '',
    buttonText: 'Загрузить еще'
};

export default TagsGrid;
