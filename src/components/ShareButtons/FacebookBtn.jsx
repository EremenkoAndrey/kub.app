import React from 'react';
import PropTypes from 'prop-types';

const FacebookBtn = ({ pageTitle, pageUrl }) => {
    const config = {
        shareUrl: 'https://www.facebook.com/sharer.php?',
        popupWidth: 650,
        buttonText: 'Поделиться в Facebook'
    };

    const url = `${config.shareUrl}src=sp&u=${
        encodeURIComponent(pageUrl)
    }&title=${
        encodeURIComponent(pageTitle)
    }`;

    const showPopup = (e) => {
        e.preventDefault();
        const winParams = `toolbar=0, 
                            status=0, 
                            width=${config.popupWidth}, 
                            height=450,
                            top=200,
                            left=${(window.innerWidth / 2) - (config.popupWidth / 2)}`;
        window.open(url, '', winParams);
    };

    return (
        <a
            href={pageUrl}
            rel="noopener noreferrer"
            target="_blank"
            className="share-buttons__link share-buttons__link_facebook"
            onClick={showPopup}
        >
            {config.buttonText}
        </a>
    );
};

FacebookBtn.propTypes = {
    pageUrl: PropTypes.string.isRequired,
    pageTitle: PropTypes.string
};

FacebookBtn.defaultProps = {
    pageTitle: ''
};

export default FacebookBtn;
