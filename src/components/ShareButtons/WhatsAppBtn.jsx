import React from 'react';
import PropTypes from 'prop-types';

const WhatsAppBtn = ({ pageTitle, pageUrl }) => {
    const config = {
        shareUrl: 'https://api.whatsapp.com/send?',
        buttonText: 'Поделиться в WhatsApp'
    };

    const concatenatedText = `${pageTitle} ${pageUrl}`;
    const url = `${config.shareUrl}text=${encodeURIComponent(concatenatedText)}`;

    const showPopup = (e) => {
        e.preventDefault();
        window.open(url);
    };

    return (
        <a
            href={pageUrl}
            target="_blank"
            className="share-buttons__link share-buttons__link_whatsapp"
            onClick={showPopup}
        >
            {config.buttonText}
        </a>
    );
};

WhatsAppBtn.propTypes = {
    pageUrl: PropTypes.string.isRequired,
    pageTitle: PropTypes.string
};

WhatsAppBtn.defaultProps = {
    pageTitle: ''
};

export default WhatsAppBtn;
