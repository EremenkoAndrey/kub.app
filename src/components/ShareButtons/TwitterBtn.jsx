import React from 'react';
import PropTypes from 'prop-types';

const TwitterBtn = ({ pageTitle, pageUrl, tags }) => {
    const config = {
        shareUrl: 'https://twitter.com/intent/tweet?',
        title: `${pageTitle} - КУБ`,
        popupWidth: 650,
        buttonText: 'Поделиться в Twitter'
    };

    const url = `${config.shareUrl}text=${
        encodeURIComponent(config.title)
    }&url=${
        encodeURIComponent(pageUrl)
    }&hashtags=${
        encodeURIComponent(tags)
    }`;

    const showPopup = (e) => {
        e.preventDefault();
        const winParams = `toolbar=0, 
                            status=0, 
                            width=${config.popupWidth}, 
                            height=450,
                            top=200,
                            left=${(window.innerWidth / 2) - (config.popupWidth / 2)}`;
        window.open(url, '', winParams);
    };

    return (
        <a
            href={url}
            target="_blank"
            className="share-buttons__link share-buttons__link_twitter"
            onClick={showPopup}
        >
            {config.buttonText}
        </a>
    );
};

TwitterBtn.propTypes = {
    pageUrl: PropTypes.string.isRequired,
    pageTitle: PropTypes.string,
    tags: PropTypes.string
};

TwitterBtn.defaultProps = {
    pageTitle: '',
    tags: ''
};

export default TwitterBtn;
