import React from 'react';
import PropTypes from 'prop-types';

const VkBtn = ({ pageTitle, pageUrl }) => {
    const config = {
        shareUrl: 'https://vk.com/share.php?',
        popupWidth: 650,
        buttonText: 'Поделиться Вконтакте'
    };

    const url = `${config.shareUrl}&title=${
        encodeURIComponent(pageTitle)
    }&url=${
        encodeURIComponent(pageUrl)
    }`;

    const showPopup = (e) => {
        e.preventDefault();
        const winParams = `toolbar=0, 
                            status=0, 
                            width=${config.popupWidth}, 
                            height=450,
                            top=200,
                            left=${(window.innerWidth / 2) - (config.popupWidth / 2)}`;
        window.open(url, '', winParams);
    };

    return (
        <a
            href={pageUrl}
            rel="noopener noreferrer"
            target="_blank"
            className="share-buttons__link share-buttons__link_vk"
            onClick={showPopup}
        >
            {config.buttonText}
        </a>
    );
};

VkBtn.propTypes = {
    pageUrl: PropTypes.string.isRequired,
    pageTitle: PropTypes.string
};

VkBtn.defaultProps = {
    pageTitle: ''
};

export default VkBtn;
