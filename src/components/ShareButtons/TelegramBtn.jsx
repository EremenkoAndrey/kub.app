import React from 'react';
import PropTypes from 'prop-types';

const TelegramBtn = ({ pageTitle, pageUrl }) => {
    const config = {
        shareUrl: 'https://telegram.me/share/url?',
        popupWidth: 650,
        buttonText: 'Поделиться в Telegram'
    };

    const url = `${config.shareUrl}url=${
        encodeURIComponent(pageUrl)
    }&text=${
        encodeURIComponent(pageTitle)
    }`;

    const showPopup = (e) => {
        e.preventDefault();
        const winParams = `toolbar=0, 
                            status=0, 
                            width=${config.popupWidth}, 
                            height=450,
                            top=200,
                            left=${(window.innerWidth / 2) - (config.popupWidth / 2)}`;
        window.open(url, '', winParams);
    };

    return (
        <a
            href={pageUrl}
            target="_blank"
            className="share-buttons__link share-buttons__link_telegram"
            onClick={showPopup}
        >
            {config.buttonText}
        </a>
    );
};

TelegramBtn.propTypes = {
    pageUrl: PropTypes.string.isRequired,
    pageTitle: PropTypes.string
};

TelegramBtn.defaultProps = {
    pageTitle: ''
};

export default TelegramBtn;
