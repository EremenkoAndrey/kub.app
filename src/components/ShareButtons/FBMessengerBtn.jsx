import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const mapStateToProps = state => ({ device: state.device });

const FBMessengerBtn = ({ device, pageUrl }) => {
    const isMobile = device.type === 'phone' || device.type === 'tablet';
    const config = {
        appId: 272487402772119,
        shareUrl: isMobile ? 'fb-messenger://share?' : 'https://www.facebook.com/dialog/send?',
        buttonText: 'Отправить в Facebook Messenger'
    };

    const url = `${config.shareUrl}link=${
        encodeURIComponent(pageUrl)
    }&app_id=${
        config.appId
    }${!isMobile ? `&redirect_uri=${encodeURIComponent(pageUrl)}` : ''}`;

    const showPopup = (e) => {
        e.preventDefault();
        window.open(url);
    };


    return (
        <a
            href={pageUrl}
            target="_blank"
            className="share-buttons__link share-buttons__link_fbmessenger"
            onClick={showPopup}
        >
            {config.buttonText}
        </a>
    );
};

FBMessengerBtn.propTypes = {
    pageUrl: PropTypes.string.isRequired,
    device: PropTypes.shape({
        type: PropTypes.string
    }).isRequired
};


export default connect(mapStateToProps)(FBMessengerBtn);
