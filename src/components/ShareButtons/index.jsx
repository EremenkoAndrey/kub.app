import React, { Component } from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies, import/first */
import FacebookBtn from './FacebookBtn';
import VkBtn from './VkBtn';
import TwitterBtn from './TwitterBtn';
import OdnoklassnikiBtn from './OdnoklassnikiBtn';
import TelegramBtn from './TelegramBtn';
import FBMessengerBtn from './FBMessengerBtn';
import WhatsAppBtn from './WhatsAppBtn';
import './index.less';

// Закомментированы элементы, отвечающие за показ/скрытие блока
export default class ShareButtons extends Component {
    constructor(props) {
        super(props);
        this.baseUrl = `${Config.get('app.protocol')}://${Config.get('app.domain')}`;
        this.state = {
            isOpen: false
        };
    }

    // onClick = () => {
    //     this.setState({
    //         isOpen: !this.state.isOpen
    //     });
    // };

    render() {
        const dynamicStyles = {
            width: this.state.isOpen ? `${this.props.buttons.length * 100}%` : ''
        };
        return (
            <div className="share-buttons">
                <div className="share-buttons__body">
                    {/* <button className="share-buttons__show-button" onClick={this.onClick} /> */}

                    {/* <div className="share-buttons__dynamic-section" style={dynamicStyles}> */}
                    <ul className="share-buttons__list">
                        {this.props.buttons.map((btnName) => {
                            let BtnComponent = null;

                            switch (btnName) {
                            case 'facebook':
                                BtnComponent = FacebookBtn;
                                break;
                            case 'vk':
                                BtnComponent = VkBtn;
                                break;
                            case 'twitter':
                                BtnComponent = TwitterBtn;
                                break;
                            case 'odnoklassniki':
                                BtnComponent = OdnoklassnikiBtn;
                                break;
                            case 'telegram':
                                BtnComponent = TelegramBtn;
                                break;
                            case 'fbmessenger':
                                BtnComponent = FBMessengerBtn;
                                break;
                            case 'whatsapp':
                                BtnComponent = WhatsAppBtn;
                                break;
                            default:
                                return null;
                            }

                            return (
                                <li className="share-buttons__item" key={btnName}>
                                    <BtnComponent
                                        pageTitle={this.props.pageTitle}
                                        pageUrl={`${this.baseUrl}${this.props.href}`}
                                        tags={this.props.tags.join(',')}
                                    />
                                </li>
                            );
                        })}
                    </ul>
                    {/* </div> */}
                </div>
            </div>
        );
    }
}

ShareButtons.propTypes = {
    buttons: PropTypes.arrayOf(PropTypes.string),
    pageTitle: PropTypes.string,
    href: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string)
};

ShareButtons.defaultProps = {
    buttons: [],
    pageTitle: '',
    href: '',
    tags: []
};
