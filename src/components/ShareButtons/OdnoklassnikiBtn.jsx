import React from 'react';
import PropTypes from 'prop-types';

const OdnoklassnikiBtn = ({ pageTitle, pageUrl }) => {
    const config = {
        shareUrl: 'https://connect.ok.ru/offer?',
        popupWidth: 650,
        buttonText: 'Поделиться в Одноклассниках'
    };

    const url = `${config.shareUrl}url=${
        encodeURIComponent(pageUrl)
    }&title=${
        encodeURIComponent(pageTitle)
    }`;

    const showPopup = (e) => {
        e.preventDefault();
        const winParams = `toolbar=0, 
                            status=0, 
                            width=${config.popupWidth}, 
                            height=450,
                            top=200,
                            left=${(window.innerWidth / 2) - (config.popupWidth / 2)}`;
        window.open(url, '', winParams);
    };

    return (
        <a
            href={pageUrl}
            target="_blank"
            className="share-buttons__link share-buttons__link_odnoklassniki"
            onClick={showPopup}
        >
            {config.buttonText}
        </a>
    );
};

OdnoklassnikiBtn.propTypes = {
    pageUrl: PropTypes.string.isRequired,
    pageTitle: PropTypes.string
};

OdnoklassnikiBtn.defaultProps = {
    pageTitle: ''
};

export default OdnoklassnikiBtn;
