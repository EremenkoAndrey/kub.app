import React from 'react';
import PropTypes from 'prop-types';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import bemNaming from 'utils/bemNaming';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';

const DecorativeTitle = ({
    elem, mods, bemNames, title, description
}) => (
    <div className={bemNames('decorative-title', elem, mods)}>
        <span className={bemNames('', 'decorative-title__title', mods)}>{title}</span>
        <span className={bemNames('', 'decorative-title__description', mods)}>{description}</span>
    </div>
);

DecorativeTitle.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    // CSS-модификатор
    mods: PropTypes.arrayOf(PropTypes.string),
    // CSS-элемент
    elem: PropTypes.string,
    // свойство composition для управления показом элементов
    bemNames: PropTypes.func
};

DecorativeTitle.defaultProps = {
    mods: [],
    elem: '',
    bemNames: bemNaming
};

export default DecorativeTitle;
