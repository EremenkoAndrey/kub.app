import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import bemNaming from 'utils/bemNaming';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */
import './index.less';


const mapStateToProps = (state, ownProps) => (
    {
        doc: state.docs[ownProps.id]
    }
);

const TagLink = connect(mapStateToProps)(({ doc }) => (
    <NavLink
        to={doc.href}
        className="tags-menu__link"
    >
        {doc.title}
    </NavLink>
));

class TagsMenu extends React.Component {
    constructor(props) {
        super(props);
        this.speed = 10;
        this.timerId = null;
        this.state = {
            showRightScrollButton: false,
            showLeftScrollButton: false
        };
    }
    componentDidMount() {
        this.buttonsVisibleControl();
    }
    componentWillUnmount() {
        clearInterval(this.timerId);
    }
    buttonsVisibleControl() {
        const scrollWidth = this.wrapper.scrollWidth - this.wrapper.offsetWidth;
        this.setState({
            showRightScrollButton: this.wrapper.scrollLeft < scrollWidth,
            showLeftScrollButton: this.wrapper.scrollLeft > 0
        });
    }

    scroll(direction) {
        switch (direction) {
        case 'right':
            this.wrapper.scrollLeft += this.speed;
            break;
        case 'left':
            this.wrapper.scrollLeft -= this.speed;
            break;
        default:
            break;
        }
    }

    moveToRight = (e) => {
        e.preventDefault();
        this.timerId = setInterval(() => {
            this.scroll('right');
            this.buttonsVisibleControl();
            if (!this.state.showRightScrollButton) {
                this.stop();
            }
        }, 20);
    };
    moveToLeft = (e) => {
        e.preventDefault();
        this.timerId = setInterval(() => {
            this.scroll('left');
            this.buttonsVisibleControl();
            if (!this.state.showLeftScrollButton) {
                this.stop();
            }
        }, 20);
    };

    stop = () => {
        clearInterval(this.timerId);
    };

    render() {
        return (
            <div className="tags-menu">
                <div className="tags-menu__wrapper" ref={(el) => { this.wrapper = el; }}>
                    <NavLink
                        to="/tags/"
                        className="tags-menu__link tags-menu__link_first"
                    >#
                    </NavLink>
                    {this.props.docs.map(id => <TagLink key={id} id={id} />)}
                </div>

                <button
                    className={this.props.bemNames('', 'tags-menu__scroller', [
                        'right',
                        `${this.state.showRightScrollButton ? 'active' : 'hide'}`
                    ])}
                    onMouseDown={this.moveToRight}
                    onTouchStart={this.moveToRight}
                    onMouseUp={this.stop}
                    onTouchEnd={this.stop}
                >
                    В начало
                </button>
                <button
                    className={this.props.bemNames('', 'tags-menu__scroller', [
                        'left',
                        `${this.state.showLeftScrollButton ? 'active' : 'hide'}`
                    ])}
                    onMouseDown={this.moveToLeft}
                    onTouchStart={this.moveToLeft}
                    onMouseUp={this.stop}
                    onTouchEnd={this.stop}
                >
                    В конец
                </button>
            </div>
        );
    }
}

TagsMenu.propTypes = {
    bemNames: PropTypes.func
};

TagsMenu.defaultProps = {
    bemNames: bemNaming
};

export default TagsMenu;
