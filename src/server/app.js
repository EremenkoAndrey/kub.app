require('./polyfills');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const slashes = require('connect-slashes');
const device = require('express-device');
const favicon = require('serve-favicon');

const app = express();
app.disable('x-powered-by');
// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
// const compiler = webpack(config);
// app.use(webpackDevMiddleware(compiler, {
//     publicPath: config.output.publicPath
// }));

// view engine setup
app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());

// uncomment after placing your favicon in /public
app.use(favicon(path.join('public', 'favicon.ico')));
app.use(logger('dev'));
app.use(express.static('public'));

app.use(bodyParser.json());
// Редирект на путь со слшем
app.use(slashes());

// Проверка типа устройства юзера и запись в req.device
app.use(device.capture());

app.use(require('./routes/'));

app.use((err, req, res, next) => {
    res.status(500).send('Something broke!');
});

module.exports = app;
