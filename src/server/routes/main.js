import express from 'express';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import initialState from 'server/initialState';
import API from 'modules/API';
import appRenderToString from 'functional/appRenderToString';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const router = express.Router();

router.get('/', (req, res) => {
    Promise.all([
        API.get('widget', ['main-promobox']).then(
            doc => Promise.resolve(doc),
            err => Promise.resolve(err)
        ),
        API.get('widget', ['main-menu']).then(
            doc => Promise.resolve(doc),
            err => Promise.resolve(err)
        ),
        API.get('widget', ['popular-news']).then(
            doc => Promise.resolve(doc),
            err => Promise.resolve(err)
        ),
        API.get('listing', ['type.Content', '12', '0']).then(
            doc => Promise.resolve(doc),
            err => Promise.resolve(err)
        )
    ])
        .then(([
            promoBox,
            tagsMenu,
            popularNews,
            newsList
        ]) => {
            // Инициализация стартовых значение store
            initialState.device.type = req.device.type.toLowerCase();
            initialState.widgets['main-promobox'] = promoBox.widget;
            initialState.widgets['main-menu'] = tagsMenu.widget;
            initialState.widgets['popular-news'] = popularNews.widget;
            initialState.listings['type.Content'] = newsList.listing;
            initialState.docs = {
                ...initialState.docs,
                ...promoBox.docs,
                ...tagsMenu.docs,
                ...popularNews.docs,
                ...newsList.docs
            };

            const html = appRenderToString(req.originalUrl, initialState);
            const helmet = Helmet.renderStatic();
            return res.render('template', { html, helmet, initialState });
        })
        .catch(err => console.log(err));
});

module.exports = router;
