import express from 'express';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import initialState from 'server/initialState';
import API from 'modules/API';
import appRenderToString from 'functional/appRenderToString';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const router = express.Router();

router.get('/', (req, res) => {
    Promise.all([
        API.get('listing', ['type.Tag', '10', '0']).then(
            doc => Promise.resolve(doc),
            err => Promise.resolve(err)
        )
    ])
        .then(([
            tagsList
        ]) => {
            // Инициализация стартовых значение store
            initialState.device.type = req.device.type.toLowerCase();
            initialState.listings['type.Tag'] = tagsList.listing;
            initialState.docs = {
                ...initialState.docs,
                ...tagsList.docs
            };

            const html = appRenderToString(req.originalUrl, initialState);
            const helmet = Helmet.renderStatic();
            return res.render('template', { html, helmet, initialState });
        });
});

router.get('/:id', (req, res, next) => {
    new Promise(resolve => API.get('document', ['tags', req.params.id])
        .then(
            doc => resolve(doc),
            err => resolve(err)
        ))
        .then((document) => {
            if (document.type === 'Redirect') {
                return res.redirect(302, document.to);
            }
            if (document.redirect) {
                return res.redirect(302, document.redirect.to);
            }

            initialState.device.type = req.device.type.toLowerCase();

            if (document.type === 'Error') {
                initialState.docs = {
                    ...initialState.docs,
                    ...{ [req.originalUrl]: document }
                };
                return next();
            }

            initialState.docs = {
                ...initialState.docs,
                ...{ [document.id]: document }
            };

            return API.get('listing', [`type.Content.tag.${document._id}`, '12', '0'])
                .then(
                    listing => Promise.resolve({ listing, id: document._id }),
                    error => Promise.resolve({ error })
                );
        })
        .then(({ listing, id, error }) => {
            if (!error) {
                initialState.listings[`type.Content.tag.${id}`] = listing.listing;
                initialState.docs = {
                    ...initialState.docs,
                    ...listing.docs
                };
            }

            const html = appRenderToString(req.originalUrl, initialState);
            const helmet = Helmet.renderStatic();
            return res.render('template', { html, helmet, initialState });
        });
});

module.exports = router;
