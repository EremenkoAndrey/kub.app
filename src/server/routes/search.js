import express from 'express';
import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import initialState from 'server/initialState';
import appRenderToString from 'functional/appRenderToString';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const router = express.Router();

router.get('/', (req, res) => {
    // Инициализация стартовых значение store
    initialState.device.type = req.device.type.toLowerCase();
    const html = appRenderToString(req.originalUrl, initialState);
    const helmet = Helmet.renderStatic();
    return res.render('template', { html, helmet, initialState });
});

module.exports = router;
