import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import initialState from 'server/initialState';
import API from 'modules/API';
import appRenderToString from 'functional/appRenderToString';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const news = (req, res, next, isPreview = false) => {
    const endpoint = isPreview ? 'preview' : 'document';
    const urlParams = isPreview ? [req.params.id, req.params.key] : ['news', req.params.id];

    Promise.all([
        API.get(endpoint, urlParams).then(
            doc => Promise.resolve(doc),
            err => Promise.resolve(err)
        ),
        API.get('listing', ['type.Content', '4', '0']).then(
            doc => Promise.resolve(doc),
            err => Promise.resolve(err)
        ),
        API.get('listing', ['type.News', '10', '0']).then(
            doc => Promise.resolve(doc),
            err => Promise.resolve(err)
        )
    ])
        .then(([
                   document,
                   contentList,
                   newsList
               ]) => {
            if (document.type === 'Redirect') {
                return res.redirect(302, document.to);
            }
            if (document.redirect) {
                return res.redirect(302, document.redirect.to);
            }

            initialState.device.type = req.device.type.toLowerCase();

            const documentId = isPreview ?
                `/preview/${req.params.id}/${req.params.key}/` :
                `${document.id}`;

            if (document.type === 'Error') {
                initialState.docs = {
                    ...initialState.docs,
                    ...{ [req.originalUrl]: document }
                };
                return next();
            }

            initialState.listings['type.Content'] = contentList.listing;
            initialState.listings['type.News'] = newsList.listing;
            initialState.docs = {
                ...initialState.docs,
                ...contentList.docs,
                ...newsList.docs,
                ...{ [documentId]: document }
            };

            const html = appRenderToString(req.originalUrl, initialState);
            const helmet = Helmet.renderStatic();
            return res.render('template', { html, helmet, initialState });
        });
};

export default news;
