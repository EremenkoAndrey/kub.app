import express from 'express';
import news from './news';

const router = express.Router();

router.use('/', require('./main'));

router.get('/news/:id', news);

router.get('/preview/:id/:key', (req, res, next) => news(req, res, next, true));

router.use('/tags', require('./tags'));

router.use('/search', require('./search'));

router.use('/fapi/v1/document', require('./api'));

router.use(require('./notFound'));

module.exports = router;
