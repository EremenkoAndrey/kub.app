import Helmet from 'react-helmet';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import initialState from 'server/initialState';
import appRenderToString from 'functional/appRenderToString';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

function notFound(req, res) {
    const html = appRenderToString(req.originalUrl, initialState);
    const helmet = Helmet.renderStatic();
    return res.status(404).render('template', { html, helmet, initialState });
}

module.exports = notFound;
