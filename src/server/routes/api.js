import express from 'express';
import request from 'request';
/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import Config from 'config/';
/* eslint-enable import/no-unresolved, import/no-extraneous-dependencies */

const router = express.Router();
const proxyHost = Config.get('api.host');
const proxyProtocol = Config.get('api.protocol');

router.get('/widget/:id', (req, res) => {
    console.log('Use app proxy widget');
    const proxyUrl = Config.get('api.widget');
    const url = `${proxyProtocol}://${proxyHost}${proxyUrl}${req.params.id}/`;
    return request(url).pipe(res);
});

router.get('/listing/:id/:quantity/:page', (req, res) => {
    console.log('Use app proxy listing');
    const proxyUrl = Config.get('api.listing');
    const url = `${proxyProtocol}://${proxyHost}${proxyUrl}${req.params.id}/${req.params.quantity}/${req.params.page}/`;
    return request(url).pipe(res);
});

router.get('/view/:type/:href', (req, res) => {
    console.log('Use app proxy document');
    const proxyUrl = Config.get('api.document');
    const url = `${proxyProtocol}://${proxyHost}${proxyUrl}${req.params.type}/${req.params.href}/`;
    return request(url).pipe(res);
});

router.get('/search/', (req, res) => {
    console.log('Use app proxy search');
    const proxyUrl = Config.get('api.search');
    const url = `${proxyProtocol}://${proxyHost}${proxyUrl}?${req.originalUrl.split('?')[1] || ''}`;
    return request(url).pipe(res);
});

module.exports = router;
