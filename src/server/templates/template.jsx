import React from 'react';
import PropTypes from 'prop-types';

const Template = props => (
    <html lang="ru" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
        <head>
            <meta charSet="UTF-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="msapplication-tap-highlight" content="no" />
            <link rel="shortcut icon" href="/favicon.ico" />
            {props.helmet.meta.toComponent()}
            <link href="/styles/styles.css" rel="stylesheet" />
            {props.helmet.title.toComponent()}
        </head>
        <body>
            <div id="kub" dangerouslySetInnerHTML={{ __html: props.html }} />
            <script
                dangerouslySetInnerHTML={{ __html: `window.__APP_INITIAL_STATE__ = ${JSON.stringify(props.initialState).replace(/</g, '\\u003c')}` }}
            />
            <script type="text/javascript" src="/index.js" />
        </body>
    </html>
);

Template.defaultProps = {
    html: ''
};

Template.propTypes = {
    html: PropTypes.string,
    helmet: PropTypes.shape({
        title: PropTypes.shape({
            toComponent: PropTypes.func,
            toString: PropTypes.func
        }),
        meta: PropTypes.shape({
            toComponent: PropTypes.func,
            toString: PropTypes.func
        })
    }).isRequired,
    initialState: PropTypes.shape().isRequired
};

module.exports = Template;
