const moduleAlias = require('module-alias');
const path = require('path');

const rootPath = path.join(path.resolve(__dirname), '../../');

moduleAlias.addAliases({
    config: path.join(rootPath, 'config'),
    'dev-data': path.join(rootPath, 'dev-data'),
    components: path.join(rootPath, 'src/components'),
    layouts: path.join(rootPath, 'src/layouts'),
    nodes: path.join(rootPath, 'src/nodes'),
    routes: path.join(rootPath, 'src/routes'),
    functional: path.join(rootPath, 'src/functional'),
    reducers: path.join(rootPath, 'src/reducers'),
    actions: path.join(rootPath, 'src/actions'),
    modules: path.join(rootPath, 'src/modules'),
    middleware: path.join(rootPath, 'src/middleware'),
    utils: path.join(rootPath, 'src/utils'),
    server: path.join(rootPath, 'src/server'),
    assets: path.join(rootPath, 'src/assets'),
    public: path.join(rootPath, 'public')
});
