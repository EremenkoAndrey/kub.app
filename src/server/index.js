const http = require('http');
const app = require('./app');
import Config from 'config/';

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    const _port = parseInt(val, 10);

    if (Number.isNaN(_port)) {
        // named pipe
        return val;
    }

    if (_port >= 0) {
        // port number
        return _port;
    }

    return false;
}



/**
 * Create HTTP server.
 */
const port = normalizePort(Config.get('app.port', '8080'));
app.set('port', port);

module.exports = {
    server: http.createServer(app),
    host: Config.get('app.host', 'localhost'),
    port
};
