/*
* Преобразование react-компонентов и конструкций, не поддерживаемых Node
 */

require('babel-core/register');

['.css', '.less', '.sass', '.ttf', '.woff', '.woff2'].forEach(ext => require.extensions[ext] = () => {});

