module.exports = {
/**
 * Application configuration section
 * http://pm2.keymetrics.io/docs/usage/application-declaration/
 */
    apps: [

    // First application
        {
            name: 'kub.media',
            script: 'start.js',
            watch: true,
            env: {
                COMMON_VARIABLE: 'true',
                NODE_ENV: 'production'
            },
            env_production: {
                NODE_ENV: 'production'
            }
        }

    // Second application
    // {
    //   name      : 'WEB',
    //   script    : 'web.js'
    // }
    ]
};
